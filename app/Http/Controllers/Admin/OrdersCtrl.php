<?php

namespace App\Http\Controllers\Admin;
use App\Model\Order;
use App\Model\Status;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class OrdersCtrl extends Controller
{
    public function orderList() {

        $sql = DB::table('orders')
            ->leftJoin('users', 'orders.id_user', '=', 'users.id')
            ->leftJoin('products', 'orders.id_product', '=', 'products.id')
            ->leftJoin('payment_methods', 'orders.id_payment', '=', 'payment_methods.id')
            ->leftJoin('statuses', 'orders.id_status', '=', 'statuses.id')
            ->select('orders.id', 'orders.count',
                'users.id as userId', 'users.name as userName',
                'products.id as prId', 'products.title as prTitle', 'products.price as prPrice',
                'statuses.id as stId', 'statuses.status_type as stType',
                'payment_methods.id as pmId', 'payment_methods.method as mtd',
                'orders.shipping_address', 'orders.expected_date', 'orders.created_at')
            ->groupBy('orders.id')
            ->get();

        $arr = array();
        foreach ($sql as $value) {
            $arr[$value->id] = $value->userName;
        }

        return response()->json(['obj' => $sql, 'arr'=>$arr]);

    }

    public function orderStatusesGet() {
        $sql = Status::all();
        return json_encode($sql);

    }

    public function orderStatusesUpdate(Request $request) {
        $orderId = $request->input('orderId');
        $statusId = $request->input('statusId');
        if($statusId == 6){
            DB::table('orders')->where('id', '=', $orderId)->delete();
        }else{
            $order = Order::find($orderId);
            $order->id_status = $statusId;
            $order->save();
        }
    }

    public function orderCardShow(Request $request) {
        $orderID = $request->input('id');
        $sql = DB::table('orders')
            ->leftJoin('users', 'orders.id_user', '=', 'users.id')
            ->leftJoin('products', 'orders.id_product', '=', 'products.id')
            ->leftJoin('payment_methods', 'orders.id_payment', '=', 'payment_methods.id')
            ->leftJoin('statuses', 'orders.id_status', '=', 'statuses.id')
            ->select('orders.id',
                'users.id as userId', 'users.name as userName', 'users.email as userEmail', 'users.phone as userPhone',
                'products.id as prId', 'products.title as prTitle', 'products.price as prPrice', 'products.photo as prPhoto',
                'statuses.id as stId', 'statuses.status_type as stType',
                'payment_methods.id as pmId', 'payment_methods.method as mtd',
                'orders.count as orCount','orders.shipping_address', 'orders.shipping_address_house_number', 'orders.shipping_address_room_number', 'orders.shipping_additional_phone', 'orders.expected_date', 'orders.created_at' )
            ->where('orders.id', '=', $orderID)
            ->get();
        return $sql;

    }

    public function orderDelete(Request $request) {
        $data = $request->input('id');
        DB::table('orders')->where('id', '=', $data)->delete();
    }
}
