<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use DB;
use Auth;
use App\Model\Shortcake;
use Illuminate\Http\Request;

class ShortcakeCtrl extends Controller
{

    public function shortcakeList() {
        $sql = array();
        $sql = DB::table('shortcakes')->select('id', 'shortcake')->get();
        $arr = array();
        foreach ($sql as $value) {
            $arr[$value->id] = $value->shortcake;
        }
        $response = array();
        $response = array('obj'=> $sql,
            'arr' => $arr);


        return response()->json(['obj' => $sql, 'arr'=>$arr]);
    }

    public function shortcakeAdd(Request $request) {
        $data = $request->input('shortcake');
        DB::table('shortcakes')->insertGetId(
            [
                'shortcake' => $data
            ]
        );

    }

    public function shortcakeUpdate(Request $request) {
        $id = $request->input('id');
        $shortcakeName = $request->input('shortcake');
        $shortcake = Shortcake::find($id);
        $shortcake->id = $id;
        $shortcake->shortcake = $shortcakeName;
        $shortcake->editor = Auth::user()->id;
        $shortcake->save();
    }

    public function shortcakeDelete(Request $request) {
        $data = $request->input('id');
        DB::table('shortcakes')->where('id', '=', $data)->delete();
    }
}
