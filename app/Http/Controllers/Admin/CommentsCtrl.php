<?php

namespace App\Http\Controllers\Admin;
use App\Model\Comment;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class CommentsCtrl extends Controller
{
    public function commentList() {

        $sql = DB::table('comments')
            ->leftJoin('users', 'comments.id_user', '=', 'users.id')
            ->leftJoin('products', 'comments.id_product', '=', 'products.id')
            ->leftJoin('comment_statuses', 'comments.id_status', '=', 'comment_statuses.id')
            ->select('comments.id', 'comments.id_user', 'comments.id_product', 'comments.id_status', 'comments.comment', 'comments.created_at', 'comment_statuses.status as cmSt', 'comment_statuses.status', 'products.title as prTitle', 'users.name as userName')
            ->groupBy('comments.id')
            ->get();

        return response()->json(['obj' => $sql]);

    }

    public function commentStatusesGet() {
        $sql = DB::table('comment_statuses')
            ->select('id', 'status')
            ->get();
        return json_encode($sql);

    }

    public function commentStatusesUpdate(Request $request) {
        $commentId = $request->input('commentId');
        $statusId = $request->input('statusId');
        if($statusId == '2'){
            DB::table('comments')->where('id', '=', $commentId)->delete();
        }else{
            $comment = Comment::find($commentId);
            $comment->id_status = $statusId;
            $comment->save();
        }
    }

    public function commentDelete(Request $request) {
        $data = $request->input('id');
        DB::table('comments')->where('id', '=', $data)->delete();
    }

    public function commentCardShow(Request $request) {
        $commentID = $request->input('id');
        $sql = DB::table('comments')
            ->leftJoin('users', 'comments.id_user', '=', 'users.id')
            ->leftJoin('products', 'comments.id_product', '=', 'products.id')
            ->leftJoin('comment_statuses', 'comments.id_status', '=', 'comment_statuses.id')
            ->select('comments.id',
                'users.id as userId', 'users.name as userName', 'users.email as userEmail',
                'products.id as prId', 'products.title as prTitle', 'products.price as prPrice', 'products.photo as prPhoto',
                'comment_statuses.id as stId', 'comment_statuses.status as stType',
                'comments.comment', 'comments.created_at' )
            ->where('comments.id', '=', $commentID)
            ->get();
        return $sql;
    }

    public function commentUpdate(Request $request) {
        $comment = $request->input();

        $commentData = Comment::find($comment['id']);
        $commentData->comment = $comment['comment'];
        $commentData->save();
    }
}
