<?php

namespace App\Http\Controllers\Admin;
use App\Model\Product;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class ProductsCtrl extends Controller
{

    public function productList() {
        $sql = DB::table('products')
            ->leftJoin('categories', 'products.id_category', '=', 'categories.id')
            ->leftJoin('creams', 'products.id_cream', '=', 'creams.id')
            ->leftJoin('shortcakes', 'products.id_shortcake', '=', 'shortcakes.id')
            ->select('products.id', 'categories.id as catId', 'categories.title as cat', 'creams.id as crId', 'creams.cream', 'shortcakes.id as cakeID', 'shortcakes.shortcake', 'products.title', 'products.price', 'products.count', 'products.photo', 'products.description', 'products.weight', 'products.availability', 'products.created_at')->get();
        $arr = array();
        foreach ($sql as $value) {
            $arr[$value->id] = $value->title;
        }


        return response()->json(['obj' => $sql, 'arr'=>$arr]);
    }

    public function productCategoryGet() {
        $sql = Product::all();
        return json_encode($sql);

    }

    public function productCardShow(Request $request) {
        $orderID = $request->input('id');
        $sql = DB::table('products')
            ->leftJoin('categories', 'products.id_category', '=', 'categories.id')
            ->leftJoin('creams', 'products.id_cream', '=', 'creams.id')
            ->leftJoin('shortcakes', 'products.id_shortcake', '=', 'shortcakes.id')
            ->select('products.id', 'categories.id as catId', 'categories.title as cat', 'creams.id as crId', 'creams.cream', 'shortcakes.id as cakeID', 'shortcakes.shortcake', 'products.title', 'products.price', 'products.count', 'products.photo', 'products.description', 'products.weight', 'products.availability', 'products.created_at')
            ->where('products.id', '=', $orderID)
            ->get();
        return $sql;
    }
    public function optionList() {
        $cream = DB::table('creams')->select('id', 'cream')->get();
        $shortcake = DB::table('shortcakes')->select('id', 'shortcake')->get();
        return response()->json(['cream' => $cream, 'cake'=>$shortcake]);
    }
    public function productUpdate(Request $request){
        $data = $request->input('product');

        $imageName = $request->file('file')->getClientOriginalName();
        $imagePath = "images/products/" . $data['title'] . "/";
        $image = $request->file('file')->move($imagePath, $imageName);

        $product = Product::find($data['id']);
        $product->title = $data['title'];
        $product->id_category = $data['catId'];
        $product->id_cream = $data['crId'];
        $product->id_shortcake = $data['cakeID'];
        $product->weight = $data['weight'];
        $product->description = $data['description'];
        $product->price = $data['price'];
        $product->photo = '/' . $imagePath . $imageName;
        $product->save();
    }
    public function productAdd(Request $request){
        $data = $request->input('product');
        $categoryName = DB::table('categories')->where('id', '=', $data['catId'])->get();
        $imageName = $request->file('file')->getClientOriginalName();
        $productName = $data['title'];
        $imagePath = "images/products/" . $categoryName[0]->title. "/" . $productName . "/";
        $image = $request->file('file')->move($imagePath, $imageName);

        $sql = DB::table('products')->insertGetId(
            [   'title' => $data['title'],
                'id_category' => $data['catId'],
                'id_cream' => $data['crId'],
                'id_shortcake' => $data['cakeID'],
                'weight' => $data['weight'],
                'description' => $data['description'],
                'price' => $data['price'],
                'photo' => '/' . $imagePath . $imageName,
                'created_at' => Carbon::now()
            ]
        );
        return json_encode($imagePath);
    }
    public function productDelete(Request $request) {
        $data = $request->input('id');
        DB::table('products')->where('id', '=', $data)->delete();
    }
}
