<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use DB;
use App\Model\Cream;
use Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class CreamsCtrl extends Controller
{
    public function creamsList() {
        $sql = array();
        $sql = DB::table('creams')->select('id', 'cream')->get();
        $arr = array();
        foreach ($sql as $value) {
            $arr[$value->id] = $value->cream;
        }
        $response = array();
        $response = array('obj'=> $sql,
            'arr' => $arr);


        return response()->json(['obj' => $sql, 'arr'=>$arr]);
    }

    public function creamAdd(Request $request) {
        $data = $request->input('cream');
        DB::table('creams')->insertGetId(
            [
            'cream' => $data
            ]
        );
    }

    public function creamUpdate(Request $request) {
        $id = $request->input('id');
        $creamName = $request->input('cream');
        $cream = Cream::find($id);
        $cream->id = $id;
        $cream->cream = $creamName;
        $cream->editor = Auth::user()->id;
        $cream->save();
    }

    public function creamDelete(Request $request) {
        $data = $request->input('id');
        DB::table('creams')->where('id', '=', $data)->delete();
    }
}
