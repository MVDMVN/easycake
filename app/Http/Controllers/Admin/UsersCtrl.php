<?php

namespace App\Http\Controllers\Admin;
use App\Model\User;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class UsersCtrl extends Controller
{

    public function userList(){
        $sql = DB::table('users')->select('id', 'name', 'email', 'phone', 'role' )->get();

        return $sql;

    }

    public function userRoleUpdate(Request $request) {
        $userId = $request->input('userID');
        $roleId = $request->input('roleID');
        $user = User::find($userId);
        $user->role = $roleId;
        $user->save();
    }

    public function userDelete(Request $request) {
        $data = $request->input('id');
        DB::table('users')->where('id', '=', $data)->delete();
    }
}
