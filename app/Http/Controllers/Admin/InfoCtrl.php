<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Auth;
use App\Model\Infos;

class InfoCtrl extends Controller
{
       public function infoList() {
        $sql = array();
        $sql = DB::table('infos')->select('id', 'title', 'info', 'editor', 'created_at')->get();
        $arr = array();
        foreach ($sql as $value) {
            $arr[$value->id] = $value->title;
        }
        $response = array();
        $response = array('obj'=> $sql,
            'arr' => $arr);


        return response()->json(['obj' => $sql, 'arr'=>$arr]);
    }

    public function infoAdd(Request $request) {
        $dataTitle = $request->input('title');
        $dataInfo = $request->input('info');
        DB::table('infos')->insertGetId(
            [
            'title' => $dataTitle,
            'info' => $dataInfo,
            'created_at' => Carbon::now()
            ]
        );
    }

       public function infoGet(Request $request) {
            $id = $request->input('id');
            $sql = DB::table('infos')
                        ->select('id', 'title', 'info')
                        ->where('id', '=', $id)
                        ->get();
            return response()->json(['obj' => $sql]);
       }

    public function infoUpdate(Request $request) {
        $id = $request->input('id');
        $title = $request->input('title');
        $dataInfo = $request->input('info');
        $info= Infos::find($id);
        $info->id = $id;
        $info->title = $title;
        $info->info = $dataInfo;
        $info->save();
    }

    public function infoDelete(Request $request) {
        $data = $request->input('id');
        DB::table('info')->where('id', '=', $data)->delete();
    }
}
