<?php

namespace App\Http\Controllers\Admin;

use App\Model\Category;
use App\Http\Controllers\Controller;
use App\Model\Order;
use App\Model\Product;
use App\Model\User;
use DB;
use Illuminate\Http\Request;
use App\Http\Requests;
use Carbon\Carbon;

class DashboardCtrl extends Controller
{
    public function dashboardList(){

        $product = Product::all()->count();
        $order = Order::all()->count();
        $user = User::all()->count();
        $category = Category::all()->count();
        
        $orderPrice = DB::select('SELECT
                SUM(oi.count * p.price) AS total_qty
          FROM orders oi
          JOIN products p ON p.id = oi.id_product');

        $sql = DB::select('
                      SELECT `id`, `title`, (
              SELECT COUNT(*)
              FROM   `products`
              WHERE  `id_category` IN (
                SELECT `id`
                FROM   `categories` AS `c2`
                WHERE  `c2`.`id` = `c`.`id`
                OR     `c2`.`id`  = `c`.`id`
              )
            ) AS `products_count`
            FROM `categories` AS `c`
          ');

        $lastComments = DB::table('comments')
            ->leftJoin('products', 'comments.id_product', '=', 'products.id')
            ->leftJoin('users', 'comments.id_user', '=', 'users.id')
            ->leftJoin('comment_statuses', 'comments.id_status', '=', 'comment_statuses.id')
            ->select('comments.id', 'comments.comment', 'comments.created_at', 'products.id_category', 'products.title as prTitle', 'products.photo', 'users.name as usrName', 'comment_statuses.status')
            ->where('comments.id_status', '=', 3)
            ->orderBy('comments.created_at', 'DESC')
            ->take(4)
            ->get();

        return response()->json(['product' => $product, 'order'=>$order, 'user'=>$user, 'category'=>$category, 'totalSum'=>$orderPrice, 'lastComments' => $lastComments, 'sql'=>$sql]);
    }
}
