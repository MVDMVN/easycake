<?php

namespace App\Http\Controllers\Admin;

use App\Model\Category;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class CategoryCtrl extends Controller
{
    public function categoryList() {
        $sql = array();
        $sql = DB::table('categories')->select('id', 'title', 'photo', 'color')->get();
        $title = DB::table('categories')->select('title')->get();
        $arr = array();
        foreach ($sql as $value) {
            $arr[$value->id] = $value->title;
        }
        $response = array();
        $response = array('obj'=> $sql,
            'arr' => $arr);


        return response()->json(['obj' => $sql, 'arr'=>$arr, 'title' => $title]);
    }

    public function categoryAdd(Request $request) {
        $data = $request->input();

        $imageName = $request->file('file')->getClientOriginalName();
        $imagePath = "images/categories/" . $data['title']. "/";
        $image = $request->file('file')->move($imagePath, $imageName);

        $sql = DB::table('categories')->insertGetId(
            [   'title' => $data['title'],
                'color' => $data['color'],
                'photo' => '/' . $imagePath . $imageName,
                'created_at' => Carbon::now()
            ]
        );
    }


    public function categoryCardShow(Request $request) {
        $categoryID = $request->input('id');
        $sql = DB::table('categories')
            ->select('categories.id', 'categories.title', 'categories.photo', 'categories.color')
            ->where('categories.id', '=', $categoryID)
            ->get();
        return $sql;
    }

    public function categoryUpdate(Request $request) {
        $data = $request->input('category');
        $imageName = $request->file('file')->getClientOriginalName();
        $imagePath = "images/categories/" . $data['title'] . "/";
        $image = $request->file('file')->move($imagePath, $imageName);

        $category = Category::find($data['id']);
        $category->title = $data['title'];
        $category->color = $data['color'];
        $category->photo = '/' . $imagePath . $imageName;
        $category->save();
    }

    public function categoryDelete(Request $request) {
        $data = $request->input('id');
        DB::table('categories')->where('id', '=', $data)->delete();
    }
}
