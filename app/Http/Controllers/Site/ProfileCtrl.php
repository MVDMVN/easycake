<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use Auth;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class ProfileCtrl extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function historyShow($name){

        setlocale(LC_ALL, 'ru_RU.UTF-8');
        $ordersHistory = DB::select('SELECT orders.id, products.photo, products.title, products.price, orders.count, orders.shipping_address, orders.created_at
                                FROM orders
                                LEFT JOIN products ON orders.id_product = products.id
                                WHERE id_user = :id
                                ORDER BY created_at;', ['id'=>Auth::user()->id]);
        return view('auth.history', ['ordersHistories'=> $ordersHistory]);
    }


    public function editShow($name){

        $user = Auth::user();
        return view('auth.edit', ['user' => $user ]);
    }


    public function updateUser(Request $request){
        $name = Auth::user();
        $name->name = $request->input('name');
        $name->email = $request->input('email');
        $name->phone = $request->input('phone');
        $name->password = $request->input('password');
        $name -> save();
        return view('auth.edit_confirm', ['nameUsers'=> $name]);
    }
}
