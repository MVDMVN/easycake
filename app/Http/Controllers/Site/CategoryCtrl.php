<?php

namespace App\Http\Controllers\Site;
use Auth;
use DB;
use Illuminate\Http\Request;
use Session;
use Carbon\Carbon;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Model\Category;

class CategoryCtrl extends Controller
{
    /************** Category ************/
    public function categoriesShow($cat) {
        $productsCategories = DB::table('products')
                            ->leftJoin('categories', 'products.id_category', '=', 'categories.id')
                            ->leftJoin('creams', 'products.id_cream', '=', 'creams.id')
                            ->leftJoin('shortcakes', 'products.id_shortcake', '=', 'shortcakes.id')
                            ->select('products.id', 'products.title', 'products.photo', 'products.price', 'creams.cream', 'shortcakes.shortcake')
                            ->where('categories.title', '=', $cat)
                            ->get();

        $creams = DB::table('creams')
                    ->select('cream')
                    ->get();

        $shortcakes = DB::table('shortcakes')
                        ->select('shortcake')
                        ->get();

        foreach ($productsCategories as $productCategory){
            $id = array();
            array_push($id, $productCategory);
        }
        return response()->json(['items' => $productsCategories, 'creams'=>$creams, 'shortcakes'=>$shortcakes]);
    }


    public function categoryShow($cat) {
        $sideMenus = Category::lists('title');

        $productRating = DB::select('SELECT round(AVG(rating.rating)) as rating
                                      FROM rating
                                      LEFT JOIN products ON rating.id_product = products.id');

        return view('products.categories', ['categories' => $cat, 'sideMenus' => $sideMenus, 'rating' => $productRating]);
    }

    public function itemsShow($cat, $title){
        $sideMenus = Category::lists('title');

        $productRating = DB::select('SELECT round(AVG(rating.rating)) as rating
                                      FROM rating
                                      LEFT JOIN products ON rating.id_product = products.id
                                      WHERE products.title = :title', ['title'=> $title]);


        $productItems = DB::table('products')
                        ->select('products.id', 'products.title', 'products.photo', 'products.description', 'products.id_cream', 'products.id_shortcake', 'products.price', 'products.weight')
                        ->where('products.title', '=', $title)
                        ->orderBy('created_at')
                        ->get();

        $creamsItems = DB::select('SELECT creams.cream
                                    FROM creams
                                    WHERE creams.id = :id;', ['id' => $productItems[0]->id_cream]);

        $shortcakeItems = DB::select('SELECT shortcakes.shortcake
                                    FROM shortcakes
                                    WHERE shortcakes.id = :id;', ['id' => $productItems[0]->id_shortcake]);


        $comments = DB::table('comments')
            ->leftJoin('users', 'comments.id_user', '=', 'users.id')
            ->select('id_user', 'id_product', 'comments.id_status', 'users.name as usrName', 'comment', 'comments.created_at')
            ->where([
            ['comments.id_status','=', 1],
            ['id_product', '=', $productItems[0]->id],
            ])
            ->get();

        return view('products.items', ['products' => $productItems, 'cream' => $creamsItems, 'shortcake' => $shortcakeItems, 'categories' => $cat, 'sideMenus' => $sideMenus, 'comments'=>$comments, 'rating' => $productRating]);

    }

    public function CommentAdd(){
        if($_POST != null){
            $sql = DB::table('comments')->insertGetId(
                [
                    'id_user' => Auth::user()->id,
                    'id_product' => $_POST['product'],
                    'id_status' => 3,
                    'comment' => $_POST['comment'],
                    'created_at' => Carbon::now()
                ]
            );
            return view('products.confirm');
        }
        else{
            return false;
        }
    }

    public function RatingSet(Request $request){
        $rating = $request->input('rating');
        $user = $request->input('user');
        $product = $request->input('product');

        DB::table('rating')->insertGetId(
            [
                'id_user' => $user,
                'id_product' => $product,
                'rating' => $rating,
                'created_at' => Carbon::now()
            ]
        );

    }
}
