<?php

namespace App\Http\Controllers\Site;
use App\Model\Category;
use App\Model\User;
use DB;
use App\Http\Requests;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Http\Controllers\Controller;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function homeShow()
    {
      // $sideMenus = Category::lists('title');
      $sideMenus =DB::table('categories')
                            ->select('title', 'photo', 'color')
                            ->get();
      $newItems = DB::table('products')
                    ->select('id', 'id_category', 'title', 'photo', 'description' )
                    ->orderBy('id', 'desc')
                    ->take(4)
                    ->get();

      $topSells = DB::select('SELECT products.id as prID, COUNT(orders.count) as total  FROM orders LEFT JOIN products ON id_product = products.id
 GROUP BY products.id ORDER BY total DESC LIMIT 4');

         foreach($topSells as $key=>$value){
             $topSellsItems[$key] =  DB::select('
    SELECT products.id, products.id_category as ctID, products.title, products.photo, products.description FROM products WHERE products.id = :id
', ['id' =>$value->prID ]);
             }

        $lastComments = DB::table('comments')
                        ->leftJoin('products', 'comments.id_product', '=', 'products.id')
                        ->leftJoin('users', 'comments.id_user', '=', 'users.id')
                        ->select('comments.id', 'comments.comment', 'comments.created_at', 'products.id_category', 'products.title as prTitle', 'products.photo', 'users.name as usrName')
                        ->orderBy('comments.created_at')
                        ->take(4)
                        ->get();


        return view('home', ['sideMenus' => $sideMenus, 'newItems' => $newItems, 'topSellsItems' => $topSellsItems, 'lastComments' => $lastComments]);

    }
}
