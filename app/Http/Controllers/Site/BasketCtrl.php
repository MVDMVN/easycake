<?php

namespace App\Http\Controllers\Site;

use Auth;
use DB;
use Exception;
use Illuminate\Http\Request;
use Session;
use Carbon\Carbon;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class BasketCtrl extends Controller
{
    public function basketShow(){
        $idData = Session::get('basket');
        $fullBaskets = array();
        $sql = array();
        $summary = 0;
        echo json_encode($fullBaskets);
        return view('auth.basket', ['basket'=>$fullBaskets, 'summary'=>$summary, 'sql'=>$sql]);
    }

    /************** Basket ************/

    public function fullBasket() {

        $idData_src = Session::get('basket');
        $idData     = array();
        if (count($idData_src) < 1) {
            throw new Exception("Пусто!");
        } else {
            foreach ($idData_src as $key => $value) {
                $i = 0;
                array_push($idData, $key);
            }
            $placeholder = implode(', ', array_fill(0, count($idData), '?'));
            $fullBaskets = DB::select('SELECT products.id, products.title, products.photo, products.price
                       FROM products
                          WHERE products.id IN ' . '(' . $placeholder . ')', $idData);

            foreach ($fullBaskets as $item) {
                $qty = $idData_src[$item->id];
                $item->qty = $qty;
            }
            echo json_encode($fullBaskets);

        }
    }

    public function basketCountGet() {
        echo count(Session::get('basket'));
    }

    public function qtyUpdate(Request $request) {
        $data         = $request->input();
        $session_Data = Session::get('basket');
        foreach ($session_Data as $key => $value) {
            if ($key == $data['id']) {
                $value = $data['qty'];
                echo json_encode($value);
            }
        }
    }

    public function basketAdd(Request $request) {

        $id  = $request->input('id');
        $qty = $request->input('qty') | 1;
        $sessionData = $request->session()->put('basket.' . $id, $qty);
        echo $sessionCount = count(Session::get('basket'));

    }

    public function basketRemove(Request $request) {

        $request->session()->forget('basket.' . $request->input('id'));

    }


    public function orderSend(Request $request) {
        $data       = $request->input();
        $basketData = Session::get('basket');

        $lastId = DB::table("orders")->select('id')
            ->orderBy('id', 'desc')
            ->take(1)
            ->get();

        $last = $lastId[0]->id;
        $last++;

        if($data['additionalPhone'] == NULL){
            $data['additionalPhone'] = "1";
        }

        if($data['additionalPhone'] == NULL){
            $data['additionalPhone'] = "1";
        }

        if($data['apartmentNumber'] == NULL){
            $data['apartmentNumber'] = "1";
        }

        foreach ($basketData as $id => $qty) {
            $orderSend = DB::select('INSERT INTO orders
                (id,
                id_user,
                id_product,
                id_payment,
                id_status,
                count,
                expected_date,
                shipping_address,
                shipping_address_house_number,
                shipping_address_room_number,
                shipping_additional_phone,
                created_at,
                updated_at)
                VALUES(' . $last . ','
                . Auth::user()->id . ','
                . $id . ','
                . 5 . ','
                . 1 . ','
                . $qty . ',"'
                . $data['date'] . '","'
                . $data['street'] . '",'
                . $data['houseNumber'] . ','
                . $data['apartmentNumber'] . ','
                . $data['additionalPhone'] . ', "'
                . Carbon::now() . '","'
                . Carbon::now() . '")');
        }

        $request->session()->forget('basket');


    }

    public function getPaymentMethods() {
        $paymentMethods = DB::table("payment_methods")
            ->get();

        return json_encode($paymentMethods);
    }
}
