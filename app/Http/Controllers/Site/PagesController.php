<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Mail;

class PagesController extends Controller
{
    public function contactShow(){
      return view('сontact');
    }

    public function contactMailSend(Request $request)
    {
    	$name = $request->input('name');
    	$email = $request->input('email');
    	$mess = $request->input('mess');

        Mail::send('emails.send', ['name' => $name, 'email' => $email, 'mess' => $mess], function ($message)
        {

            $message->from('easycake@easy.com', 'KingsMaker');

            $message->to('jenq2011@gmail.com');

        });
    }

    public function infoShow(){

    	$sql = DB::table('infos')
    		->select('id', 'title', 'info')
    		->get();
      return view('info', ['infos'=>$sql]);
    }

}
