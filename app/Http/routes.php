
<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
 */
// protected function boot(Router $router)
// {
//     parent::boot($router);
//
//     $router->model('auth', 'App\User');
// }

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
 */
Route::group(['middleware' => 'web'], function () {
    Route::auth();
    /************ Site ************/

    //Pages
    Route::get('/', ['as' => 'home', 'uses' => 'Site\HomeController@homeShow']);
    Route::get('/home', ['as' => 'home1', 'uses' => 'Site\HomeController@homeShow']);

    Route::get('контакты', 'Site\PagesController@contactShow');
    Route::post('контакты/send', 'Site\PagesController@contactMailSend');

    Route::get('информация', 'Site\PagesController@infoShow');
    Route::get('информация/get', 'Site\PagesController@infoGet');

    // Торты
    Route::get('/торты/{cat}', ['as' => 'category', 'uses' => 'Site\CategoryCtrl@categoryShow']);
    Route::get('/торты/{cat}/api', ['as' => 'categoryApi', 'uses' => 'Site\CategoryCtrl@categoriesShow']);
    Route::get('/торты/{cat}/{title}', ['as' => 'itemsPages', 'uses' => 'Site\CategoryCtrl@itemsShow']);

    Route::post('/comment', ['as' => 'comment', 'uses' => 'Site\CategoryCtrl@CommentAdd' ]);
    Route::post('/rating', 'Site\CategoryCtrl@RatingSet');

    Route::get('/admin/category', 'Admin\CategoryCtrl@categoryList');


    Route::group(['middleware' => 'auth'], function () {

    Route::get('кабинет/{name}', 'ProfileController@profileShow');
    Route::get('кабинет/{name}/история', ['as'=>'history', 'uses'=>'Site\ProfileCtrl@historyShow']);
    Route::get('кабинет/{name}/редактирование', ['as' => 'edit', 'uses' => 'Site\ProfileCtrl@editShow']);
    Route::post('кабинет/{name}/редактирование', ['as' => 'updateUser', 'uses' => 'Site\ProfileCtrl@updateUser']);
    Route::get('кабинет/{name}/избранное', ['as' => 'favorites', 'uses' => 'Site\ProfileCtrl@favoritesShow']);
    Route::get('кабинет/{name}/новое', ['as' => 'new', 'uses' => 'Site\ProfileCtrl@lastShow']);

    //Корзина
    Route::get('корзина', ['as' => 'basketShow', 'uses' => 'Site\BasketCtrl@basketShow']);
    Route::get('корзина/full', ['as' => 'fullBasket', 'uses' => 'Site\BasketCtrl@fullBasket']);
    Route::get('корзина/количество', ['as' => 'basketCountGet', 'uses' => 'Site\BasketCtrl@basketCountGet']);
    Route::post('корзина', ['as' => 'basketAdd', 'uses' => 'Site\BasketCtrl@basketAdd']);
    Route::post('корзина/удалить', ['as' => 'basketRemove', 'uses' => 'Site\BasketCtrl@basketRemove']);
    Route::post('корзина/заказать', ['as' => 'orderSend', 'uses' => 'Site\BasketCtrl@orderSend']);
    Route::post('корзина/обновить', ['as' => 'qtyUpdate', 'uses' => 'Site\BasketCtrl@qtyUpdate']);
    Route::get('корзина/оплата', ['as' => 'paymentMethods', 'uses' => 'Site\BasketCtrl@getPaymentMethods']);




    /************ Admin Panel ************/

    Route::group(['middleware' => 'admin'], function () {

    //Панель управления

    Route::get('/panel', function(){
        return view('admin.app.index');
    });


    Route::get('/admin/dashboard', 'Admin\DashboardCtrl@dashboardList');

    //Категории
    Route::post('/admin/category/add', 'Admin\CategoryCtrl@categoryAdd');
    Route::post('/admin/category/card', 'Admin\CategoryCtrl@categoryCardShow');
    Route::post('/admin/category/update', 'Admin\CategoryCtrl@categoryUpdate');
    Route::delete('/admin/category', 'Admin\CategoryCtrl@categoryDelete');

    //Товары
    Route::get('/admin/product', 'Admin\ProductsCtrl@productList');
    Route::post('/admin/product/add', 'Admin\ProductsCtrl@productAdd');
    Route::post('/admin/product/update', 'Admin\ProductsCtrl@productUpdate');
    Route::get('/admin/product/option', 'Admin\ProductsCtrl@optionList');
    Route::post('/admin/product/categoryGet', 'Admin\ProductsCtrl@productCategoryGet');
    Route::post('/admin/product/card', 'Admin\ProductsCtrl@productCardShow');
    Route::delete('/admin/product', 'Admin\ProductsCtrl@productDelete');

    //Заказы
    Route::get('/admin/order', 'Admin\OrdersCtrl@orderList');
    Route::post('/admin/order/card', 'Admin\OrdersCtrl@orderCardShow');
    Route::get('/admin/order/statusGet', 'Admin\OrdersCtrl@orderStatusesGet');
    Route::post('/admin/order/statusUpdate', 'Admin\OrdersCtrl@orderStatusesUpdate');
    Route::delete('/admin/order', 'Admin\OrdersCtrl@orderDelete');

    //Комментарии
    Route::get('/admin/comment', 'Admin\CommentsCtrl@commentList');
    Route::post('/admin/comment/card', 'Admin\CommentsCtrl@commentCardShow');
    Route::post('/admin/comment/update', 'Admin\CommentsCtrl@commentUpdate');
    Route::get('/admin/comment/statusGet', 'Admin\CommentsCtrl@commentStatusesGet');
    Route::post('/admin/comment/statusUpdate', 'Admin\CommentsCtrl@commentStatusesUpdate');
    Route::delete('/admin/comment', 'Admin\CommentsCtrl@commentDelete');

    //Пользователи
    Route::get('/admin/user', 'Admin\UsersCtrl@userList');
    Route::delete('/admin/user', 'Admin\UsersCtrl@userDelete');
    Route::post('/admin/user/roleUpdate', 'Admin\UsersCtrl@userRoleUpdate');
    Route::post('/admin/user/delete', 'Admin\UsersCtrl@userDelete');

    Route::get('/admin/shortcakes', 'Admin\ShortcakeCtrl@shortcakeList');
    Route::post('/admin/shortcake/add', 'Admin\ShortcakeCtrl@shortcakeAdd');
    Route::post('/admin/shortcake/update', 'Admin\ShortcakeCtrl@shortcakeUpdate');
    Route::delete('/admin/shortcake/delete', 'Admin\ShortcakeCtrl@shortcakeDelete');

    Route::get('/admin/creams', 'Admin\CreamsCtrl@creamsList');
    Route::post('/admin/cream/add', 'Admin\CreamsCtrl@creamAdd');
    Route::post('/admin/cream/update', 'Admin\CreamsCtrl@creamUpdate');
    Route::delete('/admin/cream/delete', 'Admin\CreamsCtrl@creamDelete');

    Route::get('/admin/info', 'Admin\InfoCtrl@infoList');
    Route::post('/admin/info/add', 'Admin\InfoCtrl@infoAdd');
    Route::post('/admin/info/get', 'Admin\InfoCtrl@infoGet');
    Route::post('/admin/info/update', 'Admin\InfoCtrl@infoUpdate');
    Route::delete('/admin/info/delete', 'Admin\InfoCtrl@infoDelete');


        });
    });

});
