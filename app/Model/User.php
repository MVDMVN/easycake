<?php

namespace App\Model;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    protected $fillable = [
        'name', 'email', 'phone', 'password',
    ];
    protected $hidden = [
        'password', 'remember_token',
    ];
}
