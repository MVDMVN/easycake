<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Shortcake extends Model
{
    protected $guarded = [
        'id',
    ];
}
