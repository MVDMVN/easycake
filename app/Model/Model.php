<?php

namespace App\Model;

use Date\DateFormat;
use Illuminate\Database\Eloquent\Model;

class Model extends Model
{
    public function getCreatedAtAttribute($attr)
    {
        return DateFormat::post($attr);
    }

}
