<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Cream extends Model
{
    protected $guarded = [
        'id',
    ];
}
