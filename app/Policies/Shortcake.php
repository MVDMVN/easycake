<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Shortcake extends Model
{
    protected $guarded = [
        'id',
    ];
}
