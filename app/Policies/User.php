<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Contracts\Auth\CanResetPassword;

class User extends Authenticatable
{
  protected $fillable = [
      'name', 'email', 'phone', 'password',
  ];
  protected $hidden = [
      'password', 'remember_token',
  ];
}
