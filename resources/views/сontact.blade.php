@extends('layouts.app-ang')
@section('content')
<!-- Parallax Start -->
<section class="parallax" id="section-3"></section>
<!-- Parallax End -->
<div class="container contacts">
    <div class="row">
        <div class="col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1">
            <div class="panel panel-default contact-panel fadeInUp hoverable z-depth-5">
            <div class="panel-heading text-center header-text">Контакты</div>
                <div class="panel-body">
                    <div class="col-lg-6 col-lg-offset-3">
                        <ul>
                            <li><i class="fa fa-map-marker"></i><span>г. Одесса ул. Польский спуск, 2</span></li>
                            <li><i class="material-icons">phone</i><span>+380934830166</span> </li>
                            <li><i class="material-icons">phone</i><span>+380937516158</span></li>
                            <li><i class="material-icons">email</i><span>easycake@cake.com </span></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Parallax Start -->
<section class="parallax" id="section-4"></section>
<!-- Parallax End -->

<div class="container contacts">
    <div class="row">
        <div class="col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1">
            <div class="panel panel-default contact-panel form fadeInUp hoverable z-depth-5">
            <div class="panel-heading text-center header-text">Написать нам</div>
                <div class="panel-body">
                    <div class="col-lg-10 col-lg-offset-1">
                            <form role="form">
                                {!! csrf_field() !!}
                                <div class="col-md-10 col-md-offset-1">
                                    <div class="form-group">
                                             <span class="input input--nao">
                                                  <input class="input__field input__field--nao" ng-model="name" id="input-1" name="name" type="text" "/>
                                                  <label class="input__label input__label--nao" for="input-1">
                                                       <span class="input__label-content input__label-content--nao">
                                                            Имя
                                                       </span>
                                                  </label>
                                                  <svg class="graphic graphic--nao" height="100%" preserveaspectratio="none" viewbox="0 0 1200 60" width="300%">
                                                       <path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0">
                                                       </path>
                                                  </svg>
                                             </span>
                                    </div>
                                    <div class="form-group">
                                             <span class="input input--nao">
                                                  <input class="input__field input__field--nao" ng-model="email" id="input-3" name="email" type="text" />
                                                  <label class="input__label input__label--nao" for="input-2">
                                                       <span class="input__label-content input__label-content--nao">
                                                            Email
                                                       </span>
                                                  </label>
                                                  <svg class="graphic graphic--nao" height="100%" preserveaspectratio="none" viewbox="0 0 1200 60" width="300%">
                                                       <path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0">
                                                       </path>
                                                  </svg>
                                             </span>
                                    </div>

                                    <div class="form-group">
                                             <span class="input input--nao">
                                                  <textarea class="input__field input__field--nao" ng-model="mess" id="message-2" name="message" type="text"></textarea> 
                                                  <label class="input__label input__label--nao" for="input-2">
                                                       <span class="input__label-content input__label-content--nao">
                                                            Сообщение
                                                       </span>
                                                  </label>
                                                  <svg class="graphic graphic--nao" height="100%" preserveaspectratio="none" viewbox="0 0 1200 60" width="300%">
                                                       <path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0">
                                                       </path>
                                                  </svg>
                                             </span>
                                    </div>

                                   <button ng-click="sendMail(name, email, mess)" class="btn btn-primary btn-md categories-button center-block ">
                                        Отправить
                                   </button>
                                </div>
                            </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Parallax Start -->
<section class="parallax" id="section-4"></section>
<!-- Parallax End -->

<div id="map-container" class="card-panel hoverable wow fadeInUp hoverable z-depth-5" style="height: 400px" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d686.7982775198279!2d30.746124315471604!3d46.48448849553497!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0000000000000000%3A0x0615d78617e79394!2z0KHQtdGA0LLQtdGAINCe0LTQtdGB0YHQutC40Lkg0JrQvtC70LvQtdC00LYg0JrQvtC80L_RjNGO0YLQtdGA0L3Ri9GFINCi0LXRhdC90L7Qu9C-0LPQuNC5!5e0!3m2!1sru!2sru!4v1457272198724"  frameborder="0" style="border:0" allowfullscreen>
</div>

<script src="{{ asset('js/angularjs/angular.js') }}">
</script>
<script src="{{ asset('js/angularjs/angular-animate.min.js') }}">
</script>
<script src="{{ asset('js/angularjs/angular-aria.min.js') }}">
</script>
<script src="{{ asset('js/angularjs/angular-messages.js') }}">
</script>
<script src="{{ asset('js/angularjs/angular-material.min.js') }}">
</script>
<script src="{{ asset('js/angularjs/angular-locale_ru-ru.js') }}">
</script>
<script src="{{ asset('js/angular-notification-icons.min.js') }}">
</script>
@endsection
