@section('sideMenu')
<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2">
                <span class="sr-only">Переключить навигацию</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand waves-effect waves-light" href="{{ route('home') }}">EasyCake</a>
            </div>
<div class="navbar navbar-fixed-top  z-depth-5 rgba-purple-strong"  >
	<ul  class="nav navbar-nav">
		@foreach($sideMenus as $sideMenu)
		<li>
			<a href="{{route('category', $sideMenu )}}">{{$sideMenu}}</a>
		</li>
		@endforeach
	</ul>
</div>
</div>
@endsection