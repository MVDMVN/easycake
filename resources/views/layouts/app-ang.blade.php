<!DOCTYPE html>
<html lang="ru" ng-app="easyCake">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>EasyCake</title>
    <!-- Favicon -->

    <link rel="icon" href="{{asset('/favicon.ico')}}" type="image/x-icon">

    <!-- Fonts -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700" rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Roboto+Mono:500,400,300,700,700italic,500italic,300italic,100italic,100,400italic|Neucha|Lobster|Comfortaa:400,700,300|Noto+Sans:400,700,400italic,700italic&subset=latin,cyrillic,cyrillic-ext' rel='stylesheet' type='text/css'>

    <!-- Styles -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
    <link href="{{ asset('css/angular-material.min.css') }}" rel="stylesheet">
    <link href="{{ asset('MDB/css/mdb.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/menu.css') }}" rel="stylesheet">
    <link href="{{ asset('css/angular-toastr.css') }}" rel="stylesheet">
    <link href="{{ asset('css/angular.rangeSlider.css') }}" rel="stylesheet">
    <link href="{{ asset('css/angular-notification-icons.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
</head>
<body id="app-layout"  ng-controller="cartController">
<nav class="navbar navbar-fixed-top z-depth-1 rgba-purple-strong"  role="navigation">
    <div class="container-fluid">
        <div class="col-lg-8">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" class="waves-effect waves-purple" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2">
                    <span class="sr-only">Переключить навигацию</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

            </div>
            <a class="navbar-brand waves-effect waves-light" href="{{ route('home') }}"><img src="{{ asset('images/logo.png') }}" alt="" title="Logo" height="40"></a>
            <div class="collapse navbar-collapse col-lg-1" id="bs-example-navbar-collapse-2">
                
                <ul class="nav navbar-nav  ">
                    <li class="active"><a href="{{ url('/') }}" class="waves-effect waves-light"><i class="fa fa-home"></i>Главная <span class="sr-only">(current)</span></a></li>
                    <li><a href="{{ url('/информация') }}" class="waves-effect waves-light"><i class="fa fa-info"></i>
                            Информация</a></li>
                    <li><a href="{{ url('/контакты') }}" class="waves-effect waves-light"><i class="fa fa-phone"></i>Контакты</a></li>
                </ul>


                <ul class="nav navbar-nav">
                    <li class="dropdown">
                    @if (Auth::guest())
                        <li><a class="waves-effect waves-purple" href="{{ url('/login') }}"><i class="fa fa-sign-in"></i>Войти</a></li>
                        <li><a  class="waves-effect waves-purple"href="{{ url('/register') }}"><i class="fa fa-user-plus"></i>Регистрация</a></li>
                    @else
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle waves-effect waves-purple" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-user "></i>
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a class="waves-effect waves-purple" href="{{ route('basketShow',Auth::user()->name) }}"><i class="material-icons">history</i>История заказов</a></li>
                                <li><a class="waves-effect waves-purple" href="{{ route('edit',Auth::user()->name) }}"><i class="material-icons">mode_edit</i>Редактировать профиль</a></li>
                                <li><a class="waves-effect waves-purple" href="{{ url('/logout') }}"><i class="material-icons">exit_to_app</i>Выйти</a></li>
                            </ul>
                        </li>
                        @endif
                        </li>
                </ul>
            </div>
            <div class="nav-phone col-md-2">
                <i class="fa fa-mobile" aria-hidden="true"></i><span>+380934830166</span>
                <br>
                <i class="fa fa-mobile" aria-hidden="true"></i><span>+380937516158</span>
            </div>

            @if (Auth::guest())
            @else
                <nav id="sidebar-menu-cart">
                    <ul ng-init="basketInit()">
                        <li>
                            <a href="{{route('basketShow',Auth::user()->name)}}">
                                <notification-icon count='basketCount' appear-animation='bounce' update-animation='shake' disappear-animation='fade'>
                                    <i class="fa fa-shopping-cart fa-3x">
                                    </i>
                                </notification-icon>
                            </a>
                        </li>
                    </ul>
                </nav>
            @endif
        </div>
    </div>
</nav>
<nav class="navbar navbar-fixed-bottom z-depth-1 rgba-purple-strong"  role="navigation">
    <div class="container-fluid">
        <div class="col-lg-12 items">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" class="waves-effect waves-purple" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2">
                    <span class="sr-only">Переключить навигацию</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <div class="collapse navbar-collapse col-lg-10" id="bs-example-navbar-collapse-2">
                <ul class="nav navbar-nav">
                    @foreach( App\Model\Category::lists('title') as $sideMenu)
                        <li><a  href="{{route('category', $sideMenu )}}">{{$sideMenu}}</a></li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>

</nav>
@yield('content')


<!-- JavaScripts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<script src="{{ asset('MDB/js/mdb.min.js') }}"></script>
<script src="{{ asset('js/angular-scroll.js') }}"></script>
<script src="{{ asset('js/classie.js') }}"></script>
<script src="{{ asset('js/basket.js') }}"></script>
<script src="{{ asset('js/comment.js') }}"></script>
<script src="{{ asset('js/slick/slick.min.js') }}"></script>
<script src="{{ asset('js/angular-slick-carousel/dist/angular-slick.min.js') }}"></script>
<script src="{{ asset('js/my_jquery.min.js') }}"></script>
<script src="{{ asset('js/angular-notification-icons.min.js') }}"></script>
<script src="{{ asset('js/angular-input-stars-directive/angular-input-stars.js') }}"></script>
<script src="{{ asset('js/angular-toastr/dist/angular-toastr.tpls.js') }}"></script>
<script src="{{ asset('js/angular-rangeslider/angular.rangeSlider.js') }}"></script>
<script src="{{ asset('js/main.js') }}"></script>

<script>

    (function(){
        var d = document,
                accordionToggles = d.querySelectorAll('.js-accordionTrigger'),
                setAria,
                setAccordionAria,
                switchAccordion,
                touchSupported = ('ontouchstart' in window),
                pointerSupported = ('pointerdown' in window);

        skipClickDelay = function(e){
            e.preventDefault();
            e.target.click();
        }

        setAriaAttr = function(el, ariaType, newProperty){
            el.setAttribute(ariaType, newProperty);
        };
        setAccordionAria = function(el1, el2, expanded){
            switch(expanded) {
                case "true":
                    setAriaAttr(el1, 'aria-expanded', 'true');
                    setAriaAttr(el2, 'aria-hidden', 'false');
                    break;
                case "false":
                    setAriaAttr(el1, 'aria-expanded', 'false');
                    setAriaAttr(el2, 'aria-hidden', 'true');
                    break;
                default:
                    break;
            }
        };
//function
        switchAccordion = function(e) {
            console.log("triggered");
            e.preventDefault();
            var thisAnswer = e.target.parentNode.nextElementSibling;
            var thisQuestion = e.target;
            if(thisAnswer.classList.contains('is-collapsed')) {
                setAccordionAria(thisQuestion, thisAnswer, 'true');
            } else {
                setAccordionAria(thisQuestion, thisAnswer, 'false');
            }
            thisQuestion.classList.toggle('is-collapsed');
            thisQuestion.classList.toggle('is-expanded');
            thisAnswer.classList.toggle('is-collapsed');
            thisAnswer.classList.toggle('is-expanded');

            thisAnswer.classList.toggle('animateIn');
        };
        for (var i=0,len=accordionToggles.length; i<len; i++) {
            if(touchSupported) {
                accordionToggles[i].addEventListener('touchstart', skipClickDelay, false);
            }
            if(pointerSupported){
                accordionToggles[i].addEventListener('pointerdown', skipClickDelay, false);
            }
            accordionToggles[i].addEventListener('click', switchAccordion, false);
        }
    })();
</script>
</body>
</html>
