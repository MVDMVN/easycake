@section('footer')
<!-- Footer section Start -->
    <footer class="page-footer center-on-small-only">
            <div class="footer-copyright text-center rgba-black-light">
                <div class="container-fluid">
                    © 2016 Copyright: <a href="#"> KingsMaker </a>
                </div>
            </div>
        </footer>
<!-- Footer section End -->
@endsection