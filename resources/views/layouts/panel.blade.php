@extends('layouts.app')
@section ('content')
<div class="container-fluid">
  <div class="row">
    <div class="col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1 col-xs-10 col-xs-offset-1  categories-panel header-margin">

          @foreach($items as $item)
        <div class="col-md-4 ">
          <div class=" col-lg-8 col-md-8 col-xs-8 col-lg-offset-2 col-md-offset-2 col-xs-offset-2 theme-1 hoverable z-depth-5 category-item">
            <div class="view overlay hm-zoom ">
                    <div class="category-image">
                        <img class="waves-effect waves-purple" src="{{$item->photo}}" title="" alt=""><br>
                        <div class="mask waves-effect waves-purple"></div>
                    </div>
            </div>
            <h5 class="category-name text-center">{{$item->title}}</h5>
            <p class="category-price text-center">Цена: {{$item->price}} грн. </p>
            <form class="form-horizontal center-block" role="form" method="POST" action="{{ route('basket')}}">
              <input name="shop" type="hidden" value="{{$item->id}}">
              <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
              <button class="btn btn-primary btn-md categories-button  ">Правка</button>
              <button class="btn btn-primary btn-md categories-button  ">Удалить</button>
            </form>
          </div>
          </div>
          @endforeach
    </div>
  </div>
</div>
@endsection
