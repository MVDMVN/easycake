@extends('layouts.app')
@section('content')
<div class="container-fluid info-page">
          <h1 class="heading-primary theme-headers">Информация для покупателей</h1>
          <div class="accordion">
            <dl>
            @foreach($infos as $info )
              <dt>
                <a href="#accordion1" aria-expanded="false" aria-controls="accordion1" class="accordion-title accordionTitle js-accordionTrigger">{{$info->title}}</a>
              </dt>
              <dd class="accordion-content accordionItem is-collapsed" id="accordion1" aria-hidden="true">
                <p>{{$info->info}}</p>
              </dd>
            @endforeach
            </dl>
          </div>
        </div>

    <script src="{{ asset('js/angularjs/angular.js') }}"></script>
    <script src="{{ asset('js/angularjs/angular-animate.min.js') }}"></script>
    <script src="{{ asset('js/angularjs/angular-aria.min.js') }}"></script>
    <script src="{{ asset('js/angularjs/angular-messages.js') }}"></script>
    <script src="{{ asset('js/angularjs/angular-material.min.js') }}"></script>
    <script src="{{ asset('js/angularjs/angular-locale_ru-ru.js') }}"></script>
@endsection
