'use strict';

app
  .controller('InfoCtrl', ['$scope', '$log', '$http', '$state', '$stateParams', '$firebaseArray', '$firebaseObject', 'FBURL',
  function($scope, $log, $http, $state, $firebaseArray, $firebaseObject, FBURL) {
  }
])

  .controller('InfoListCtrl', ['$scope', '$log', '$http', '$stateParams', '$state', '$filter', 'ngTableParams', 'toastr',
    function($scope, $log, $http, $stateParams, $state, $filter, ngTableParams, toastr) {

      //////////////////////////////////////////
      //************ Table Settings **********//
      //////////////////////////////////////////



      $scope.myInfo = function(){


        $http.get( '/admin/info' )
          .success( function(result){
            var data = result.obj;
            $scope.info = result.obj;


            $scope.tableParams = new ngTableParams({
              page: 1,            // show first page
              count: 10,          // count per page
              sorting: {
                id: 'asc'     // initial sorting
              }
            }, {
              total: data.length, // length of data
              getData: function($defer, params) {
                // use build-in angular filter
                var orderedData = params.sorting() ?
                  $filter('orderBy')(data, params.orderBy()) :
                  data;

                orderedData = $filter('filter')(orderedData, $scope.searchText);
                params.total(orderedData.length);

                $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
              }
            });
          });
      }

      $scope.myInfo();

        $scope.$watchCollection('orders', function(newVal, oldVal){
          if (newVal !== oldVal) {
            $scope.tableParams.reload();
          }
        });

        $scope.$watch('searchText', function(newVal, oldVal){
          if (newVal !== oldVal) {
            $scope.tableParams.reload();
          }
        });

        // Delete CRUD operation
        $scope.delete = function(id) {
          var idData = "id=" + id;
          if (confirm('Вы уверены?')) {
            $http.delete("/admin/info/delete", {
                data: idData,
                headers: {
                  'Content-Type': 'application/x-www-form-urlencoded'
                }
              })
              .success(function(data) {
                console.log(data);
                toastr.success('Удаление блока с информацией', 'Блок с информацией был удален!');
                $state.go('app.info.list', {}, {
                  reload: true
                });
              })
          }
        };
        //////////////////////////// *Delete CRUD operation
    }])


.controller('NewInfoCtrl', ['$scope', '$http', 'toastr', '$state', 'FBURL', '$filter', 'Upload',
    function($scope, $http, toastr, $state, FBURL, $filter, Upload) {

      $scope.infoAdd = function(title, info) {

        Upload.upload({
          url: '/admin/info/add',
          data: {title, info},
          headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
              }
        })
        .success(function (result){
                    toastr.success('Добавление блока с информацией', 'Блок с информацией был добавлен!');
          $state.go('app.info.list', {}, {
            reload: true
          });
        })
      }
    }
  ])

  .controller('EditInfoCtrl', ['$scope', '$http', '$stateParams', '$firebaseObject', 'toastr', '$state', 'FBURL', '$filter', 'Upload',
    function($scope, $http, $stateParams, $firebaseObject, toastr, $state, FBURL, $filterm, Upload) {
      var id = "id=" + $stateParams.id;
      $http( {
        method: 'POST',
        url: "/admin/info/get",
        data: id,
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        },
      } )
        .success(function(result) {
          $scope.info = result.obj[0];
          console.log($scope.info);
        })

      $scope.infoUpdate = function(id, title, info) {
        console.log(info);
        Upload.upload({
          url: '/admin/info/update',
          data: {id, title, info},
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          }
        }).success(function(result){
          toastr.success('Редактирование блока с информацией', 'Блок с информацией был отредактирован!');
          $state.go('app.info.list', {}, {
            reload: true
          });
        })
      }
    }
  ])


  .controller('ShowInfoCtrl', ['$scope', '$http', '$firebaseObject', 'toastr', '$state', 'FBURL', '$stateParams',
    function($scope, $http, $firebaseObject, toastr, $state, FBURL, $stateParams) {
      var id = "id=" + $stateParams.id;

      $http( {
        method: 'POST',
        url: "/admin/comment/card",
        data: id,
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        },
      } )
        .success(function(result) {
            $scope.comment = result[0];
          console.log($scope.comment);
        })

    }]);
