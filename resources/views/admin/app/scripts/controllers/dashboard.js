'use strict';

app
  .controller('DashboardCtrl', ['$scope', '$state', '$http', '$stateParams', '$filter', 'uploadImage', 'toastr',
    function($scope, $state, $http, $stateParams, $filter, uploadImage, toastr) {

      $scope.page = {
        title: 'Dashboard'
      };

      $http.get( '/admin/dashboard' )
      .success( function(result){
        $scope.dbData = result;
        console.log($scope.dbData);
});
    }])


  .controller('ProductsChartCtrl', ['$scope', '$filter', '$http',
    function($scope, $filter, $http) {
        var count = 0;

        $http.get('/admin/dashboard')
            .success( function(result) {
                $scope.chartData = result.sql;

                count = $scope.chartData.length;

                function getRandomColor() {
                    var letters = '0123456789ABCDEF'.split('');
                    var color = '#';
                    for (var i = 0; i < 6; i++ ) {
                        color += letters[Math.floor(Math.random() * 16)];
                    }
                    return color;
                }
                $scope.chart = {};
                    angular.forEach($scope.chartData, function(value, key) {
                        $scope.chart[key] = {
                                label: value.title,
                                color : getRandomColor(),
                                highlight : getRandomColor(),
                                value: value.products_count
                        }

                    });
            })


    }
  ])

.controller('LastCommentsCtrl', ['$scope', '$filter', '$http', 'toastr', '$state',
  function($scope, $filter, $http, toastr, $state) {
    $http.get('/admin/dashboard')
      .success( function(result) {

        $scope.lastComments = result.lastComments;

      })



    $scope.changeStatus = function (commentId, statusId) {

      var statusUpdateData = 'commentId=' + commentId + '&'
        + 'statusId=' + statusId;

      console.log(statusUpdateData);
      $http( {
        method: 'POST',
        url: "/admin/comment/statusUpdate",
        data: statusUpdateData,
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        },
      } )
        .success(function(result) {
          toastr.success('Изменение статуса', 'Статус был изменён!');
          $state.go('app.dashboard', {}, {
            reload: true
          });
        })
    };

  }
]);
