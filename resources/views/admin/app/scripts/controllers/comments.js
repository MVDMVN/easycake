'use strict';

app

  .controller('CommentCtrl', ['$scope', '$state', '$http', '$stateParams', '$firebaseArray', '$firebaseObject', 'FBURL', '$filter',
    function($scope, $state, $http, $stateParams, $firebaseArray, $firebaseObject, FBURL, $filter) {
    }])

  .controller('CommentsListCtrl', ['$scope', '$log', '$http', '$stateParams', '$state', '$filter', 'ngTableParams', 'toastr',
    function($scope, $log, $http, $stateParams, $state, $filter, ngTableParams, toastr) {

      //////////////////////////////////////////
      //************ Table Settings **********//
      //////////////////////////////////////////

      $http.get( '/admin/comment/statusGet' )
        .success( function(result){
          $scope.statuses = result;
          console.log($scope.statuses);
        });



      $scope.myComment = function(){


        $http.get( '/admin/comment' )
          .success( function(result){
            var data = result.obj;
            console.log(data);
            // angular.forEach(data, function (value, key) {
            //   data[key].expected_date = new Date(value.expected_date);
            // })


            $scope.tableParams = new ngTableParams({
              page: 1,            // show first page
              count: 10,          // count per page
              sorting: {
                id: 'asc'     // initial sorting
              }
            }, {
              total: data.length, // length of data
              getData: function($defer, params) {
                // use build-in angular filter
                var orderedData = params.sorting() ?
                  $filter('orderBy')(data, params.orderBy()) :
                  data;

                orderedData = $filter('filter')(orderedData, $scope.searchText);
                params.total(orderedData.length);

                $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
              }
            });
          });
      }

      $scope.myComment();

      $scope.changeStatus = function (commentId, statusId) {
        var statusUpdateData = 'commentId=' + commentId + '&'
                                + 'statusId=' + statusId;
        console.log(statusId);
        $http( {
          method: 'POST',
          url: "/admin/comment/statusUpdate",
          data: statusUpdateData,
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          },
        } )
          .success(function(result) {
            toastr.success('Изменение статуса', 'Статус был изменён!');
            $state.go('app.comments.list', {}, {
              reload: true
            });
          })
      };


        $scope.$watchCollection('orders', function(newVal, oldVal){
          if (newVal !== oldVal) {
            $scope.tableParams.reload();
          }
        });

        $scope.$watch('searchText', function(newVal, oldVal){
          if (newVal !== oldVal) {
            $scope.tableParams.reload();
          }
        });

        // Delete CRUD operation
        $scope.delete = function(id) {
          var idData = "id=" + id;
          if (confirm('Вы уверены?')) {
            $http.delete("/admin/comment", {
                data: idData,
                headers: {
                  'Content-Type': 'application/x-www-form-urlencoded'
                }
              })
              .success(function(data) {
                console.log(data);
                toastr.success('Удаление комментария', 'Комментарий был удален!');
                $state.go('app.comments.list', {}, {
                  reload: true
                });
              })
          }
        };
        //////////////////////////// *Delete CRUD operation





    }])


  .controller('EditCommentCtrl', ['$scope', '$http', '$stateParams', '$firebaseObject', 'toastr', '$state', 'FBURL', '$filter', 'Upload',
    function($scope, $http, $stateParams, $firebaseObject, toastr, $state, FBURL, $filterm, Upload) {
      var id = "id=" + $stateParams.id;
      $http( {
        method: 'POST',
        url: "/admin/comment/card",
        data: id,
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        },
      } )
        .success(function(result) {
          $scope.comment = result[0];
          console.log($scope.comment);
        })

      $scope.commentUpdate = function(id, comment) {
        console.log(comment);
        Upload.upload({
          url: '/admin/comment/update',
          data: {id, comment},
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          }
        }).success(function(result){
          toastr.success('Редактирование комментария', 'Комментарий был отредактирован!');
          $state.go('app.comments.list', {}, {
            reload: true
          });
        })
      }
    }
  ])


  .controller('ShowCommentCtrl', ['$scope', '$http', '$firebaseObject', 'toastr', '$state', 'FBURL', '$stateParams',
    function($scope, $http, $firebaseObject, toastr, $state, FBURL, $stateParams) {
      var id = "id=" + $stateParams.id;

      $http( {
        method: 'POST',
        url: "/admin/comment/card",
        data: id,
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        },
      } )
        .success(function(result) {
            $scope.comment = result[0];
          console.log($scope.comment);
        })

    }]);
