'use strict';

app

  .controller('CreamsCtrl', ['$scope', '$log', '$http', '$state', '$stateParams', '$firebaseArray', '$firebaseObject', 'FBURL',
  function($scope, $log, $http, $state, $firebaseArray, $firebaseObject, FBURL) {

    // General database variable
    // var ref = new Firebase(FBURL);

    // $scope.categoriesObject = $firebaseObject(ref.child('categories'));
    //////////////////////////// *General database variable




  }
])

.controller('CreamsListCtrl', ['$scope', '$log', '$http', '$state', '$filter', 'ngTableParams', 'toastr',
  function($scope, $log, $http, $state, $filter, ngTableParams, toastr) {

    //////////////////////////////////////////
    //************ Table Settings **********//
    //////////////////////////////////////////


    $scope.myCreams = function() {
      $http.get('/admin/creams')
        .success(function(result) {
          var data = result.obj;
          $scope.creams = result.obj;
          console.log($scope.creams);

          $scope.tableParams = new ngTableParams({
            page: 1, // show first page
            count: 10, // count per page
            sorting: {
              id: 'asc' // initial sorting
            }
          }, {
            total: data.length, // length of data
            getData: function($defer, params) {
              // use build-in angular filter
              var orderedData = params.sorting() ?
                $filter('orderBy')(data, params.orderBy()) :
                data;

              orderedData = $filter('filter')(orderedData, $scope.searchText);
              params.total(orderedData.length);

              $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
            }
          });
        });
    }

    $scope.myCreams();

    // watch data in scope, if change reload table
    $scope.$watchCollection('categories', function(newVal, oldVal) {
      if (newVal !== oldVal) {
        $scope.tableParams.reload();
      }
    });

    $scope.$watch('searchText', function(newVal, oldVal) {
      if (newVal !== oldVal) {
        $scope.tableParams.reload();
      }
    });
    ///////////////////////////////////////////// *watch data in scope, if change reload table


    // Delete CRUD operation
    $scope.delete = function(id) {
      var idData = "id=" + id;
      if (confirm('Вы уверены?')) {
        $http.delete("/admin/cream/delete", {
            data: idData,
            headers: {
              'Content-Type': 'application/x-www-form-urlencoded'
            }
          })
          .success(function(data) {
            console.log(data);
            toastr.success('Удаление крема', 'Крем был удален!');
            $state.go('app.creams.list', {}, {
              reload: true
            });
          })
      }
    };
    //////////////////////////// *Delete CRUD operation


  }
])

.controller('NewCreamsCtrl', ['$scope', '$http', 'toastr', '$state', 'FBURL', '$filter', 'Upload',
    function($scope, $http, toastr, $state, FBURL, $filter, Upload) {

      $scope.creamAdd = function(cream) {

        Upload.upload({
          url: '/admin/cream/add',
          data: {cream},
          headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
              }
        })
        .success(function (result){
                    toastr.success('Добавление крема', 'Крем был добавлен!');
          $state.go('app.creams.list', {}, {
            reload: true
          });
        })
      }
    }
  ])
  /////////////////////// *Submit operation


.controller('EditCreamsCtrl', ['$scope', '$http', '$stateParams', '$firebaseObject', 'toastr', '$state', 'FBURL', '$filter',
  function($scope, $http, $stateParams, $firebaseObject, toastr, $state, FBURL, $filter) {
    var id = $stateParams.id;
    $http.get('/admin/creams')
      .success(function(result) {
        $scope.cream = result.arr[id] ;
        console.log($scope.cream);
      });

    $scope.creamUpdate = function(cream) {
      var updateData = "id=" + id + "&" + "cream=" + cream;
      console.log(updateData);
      $http( {
        method: 'POST',
        url: "/admin/cream/update",
        data: updateData,
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        },
      } )
        .success(function(result) {
          toastr.success('Редактирование крема', 'Крем был изменен!');
          $state.go('app.creams.list', {}, {
            reload: true
          });
        })
    }
  }
])

.controller('ShowCreamsCtrl', ['$scope', '$http', '$firebaseObject', 'toastr', '$state', 'FBURL', '$stateParams',
  function($scope, $http, $firebaseObject, toastr, $state, FBURL, $stateParams) {

    // var id = $stateParams.id;
    // $http.get('/admin/category')
    //   .success(function(result) {
    //     $scope.categories = result.obj;
    //     $scope.category = $scope.categories[id];
    //     console.log($scope.category );
    //   });

        var id = "id=" + $stateParams.id;
        $http( {
            method: 'POST',
            url: "/admin/category/card",
            data: id,
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
        } )
            .success(function(result) {
                $scope.category = result[0];
                console.log($scope.category);
            })
  }
]);
