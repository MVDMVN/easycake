'use strict';

app

  .controller('OrdersCtrl', ['$scope', '$state', '$http', '$stateParams', '$firebaseArray', '$firebaseObject', 'FBURL', '$filter',
    function($scope, $state, $http, $stateParams, $firebaseArray, $firebaseObject, FBURL, $filter) {

      // // General database variable
      // var ref = new Firebase(FBURL);
      // $scope.orders = $firebaseArray(ref.child('orders'));
      // $scope.ordersObject = $firebaseObject(ref.child('orders'));
      //
      // $scope.categories = $firebaseArray(ref.child('categories'));
      // $scope.categoriesObject = $firebaseObject(ref.child('categories'));
      //
      // $scope.products = $firebaseArray(ref.child('products'));
      // $scope.productsObject = $firebaseObject(ref.child('products'));
      // //////////////////////////// *General database variable
      //
      // $scope.user = user;
      //
      // // get the model
      // if($stateParams.id) {
      //   var id = $stateParams.id;
      //   $scope.order = $firebaseObject(ref.child('orders').child(id));
      // } else {
      //   $scope.order = {};
      // }

    }])

  .controller('OrdersListCtrl', ['$scope', '$log', '$http', '$stateParams', '$state', '$filter', 'ngTableParams', 'toastr',
    function($scope, $log, $http, $stateParams, $state, $filter, ngTableParams, toastr) {

      //////////////////////////////////////////
      //************ Table Settings **********//
      //////////////////////////////////////////

      $http.get( '/admin/order/statusGet' )
      .success( function(result){
        $scope.statuses = result;
        console.log($scope.statuses);
      });



      $scope.myOrder = function(){


        $http.get( '/admin/order' )
          .success( function(result){
            var data = result.obj;
            console.log(data);
            angular.forEach(data, function (value, key) {
              data[key].expected_date = new Date(value.expected_date);
            })


            $scope.tableParams = new ngTableParams({
              page: 1,            // show first page
              count: 10,          // count per page
              sorting: {
                id: 'asc'     // initial sorting
              }
            }, {
              total: data.length, // length of data
              getData: function($defer, params) {
                // use build-in angular filter
                var orderedData = params.sorting() ?
                  $filter('orderBy')(data, params.orderBy()) :
                  data;

                orderedData = $filter('filter')(orderedData, $scope.searchText);
                params.total(orderedData.length);

                $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
              }
            });
          });
      }

      $scope.myOrder();

      $scope.changeStatus = function (orderId, statusId) {
        var statusUpdateData = 'orderId=' + orderId + '&'
                                + 'statusId=' + statusId;
        $http( {
          method: 'POST',
          url: "/admin/order/statusUpdate",
          data: statusUpdateData,
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          },
        } )
          .success(function(result) {
            toastr.success('Изменение статуса', 'Статус был изменён!');
            $state.go('app.orders.list', {}, {
              reload: true
            });
          })
      };

      // Initialize table
      // $scope.orders.$loaded().then(function() {

        // watch data in scope, if change reload table
        $scope.$watchCollection('orders', function(newVal, oldVal){
          if (newVal !== oldVal) {
            $scope.tableParams.reload();
          }
        });

        $scope.$watch('searchText', function(newVal, oldVal){
          if (newVal !== oldVal) {
            $scope.tableParams.reload();
          }
        });
        ///////////////////////////////////////////// *watch data in scope, if change reload table


        // Delete CRUD operation
        $scope.delete = function(id) {
          var idData = "id=" + id;
          if (confirm('Вы уверены?')) {
            $http.delete("/admin/order", {
                data: idData,
                headers: {
                  'Content-Type': 'application/x-www-form-urlencoded'
                }
              })
              .success(function(data) {
                console.log(data);
                toastr.success('Удаление заказа', 'Заказ был удален!');
                $state.go('app.orders.list', {}, {
                  reload: true
                });
              })
          }
        };
        //////////////////////////// *Delete CRUD operation





    }])


  .controller('ShowOrderCtrl', ['$scope', '$http', '$firebaseObject', 'toastr', '$state', 'FBURL', '$stateParams',
    function($scope, $http, $firebaseObject, toastr, $state, FBURL, $stateParams) {
      var id = "id=" + $stateParams.id;
      console.log(id);
      $http( {
        method: 'POST',
        url: "/admin/order/card",
        data: id,
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        },
      } )
        .success(function(result) {
            $scope.order = result[0];
          $scope.order.created_at = new Date($scope.order.created_at);
          $scope.order.expected_date = new Date($scope.order.expected_date);
          $scope.orderItems = result;
            var sum = 0;
            angular.forEach($scope.orderItems, function(value) {
                sum  = sum + (value.prPrice * value.orCount);
                $scope.Summary = sum;
            })
        })

    }]);
