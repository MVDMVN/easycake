'use strict';

app
  .constant('ROUTES', {
    'app': {
      abstract: true,
      url: '/app',
      templateUrl: 'admin/app/views/app.html',
      authRequired: false
    },
    'app.dashboard': {
      url: '/dashboard',
      controller: 'DashboardCtrl',
      templateUrl: 'admin/app/views/pages/dashboard.html',
      authRequired: false
    },

    'app.categories': {
      abstract: true,
      url: '/categories',
      template: '<div ui-view></div>',
      authRequired: false
    },
    'app.categories.list': {
      url: '/list',
      controller: 'CategoriesCtrl',
      templateUrl: 'admin/app/views/pages/categories/list.html',
      authRequired: false
    },
    'app.categories.new': {
      url: '/new',
      controller: 'CategoriesCtrl',
      templateUrl: 'admin/app/views/pages/categories/new.html',
      authRequired: false
    },
    'app.categories.edit': {
      url: '/edit/:id',
      controller: 'CategoriesCtrl',
      templateUrl: 'admin/app/views/pages/categories/edit.html',
      authRequired: false
    },
    'app.categories.show': {
      url: '/show/:id',
      controller: 'CategoriesCtrl',
      templateUrl: 'admin/app/views/pages/categories/show.html',
      authRequired: false
    },

/****************************************************************************************/

        'app.shortcakes': {
      abstract: true,
      url: '/shortcakes',
      template: '<div ui-view></div>',
      authRequired: false
    },
    'app.shortcakes.list': {
      url: '/list',
      controller: 'ShortcakesCtrl',
      templateUrl: 'admin/app/views/pages/shortcakes/list.html',
      authRequired: false
    },
    'app.shortcakes.new': {
      url: '/new',
      controller: 'ShortcakesCtrl',
      templateUrl: 'admin/app/views/pages/shortcakes/new.html',
      authRequired: false
    },
    'app.shortcakes.edit': {
      url: '/edit/:id',
      controller: 'ShortcakesCtrl',
      templateUrl: 'admin/app/views/pages/shortcakes/edit.html',
      authRequired: false
    },
    'app.shortcakes.show': {
      url: '/show/:id',
      controller: 'ShortcakesCtrl',
      templateUrl: 'admin/app/views/pages/shortcakes/show.html',
      authRequired: false
    },

        'app.creams': {
      abstract: true,
      url: '/creams',
      template: '<div ui-view></div>',
      authRequired: false
    },
    'app.creams.list': {
      url: '/list',
      controller: 'CreamsCtrl',
      templateUrl: 'admin/app/views/pages/creams/list.html',
      authRequired: false
    },
    'app.creams.new': {
      url: '/new',
      controller: 'CreamsCtrl',
      templateUrl: 'admin/app/views/pages/creams/new.html',
      authRequired: false
    },
    'app.creams.edit': {
      url: '/edit/:id',
      controller: 'CreamsCtrl',
      templateUrl: 'admin/app/views/pages/creams/edit.html',
      authRequired: false
    },
    'app.creams.show': {
      url: '/show/:id',
      controller: 'CreamsCtrl',
      templateUrl: 'admin/app/views/pages/creams/show.html',
      authRequired: false
    },



    'app.comments': {
      abstract: true,
      url: '/comments',
      template: '<div ui-view></div>',
      authRequired: false
    },
    'app.comments.list': {
      url: '/list',
      controller: 'CommentCtrl',
      templateUrl: 'admin/app/views/pages/comments/list.html',
      authRequired: false
    },
    'app.comments.show': {
      url: '/show/:id',
      controller: 'CommentCtrl',
      templateUrl: 'admin/app/views/pages/comments/show.html',
      authRequired: false
    },

    'app.comments.edit': {
      url: '/edit/:id',
      controller: 'CommentCtrl',
      templateUrl: 'admin/app/views/pages/comments/edit.html',
      authRequired: false
    },

      'app.info': {
      abstract: true,
      url: '/info',
      template: '<div ui-view></div>',
      authRequired: false
    },
    'app.info.list': {
      url: '/list',
      controller: 'InfoCtrl',
      templateUrl: 'admin/app/views/pages/info/list.html',
      authRequired: false
    },
    'app.info.new': {
      url: '/new',
      controller: 'InfoCtrl',
      templateUrl: 'admin/app/views/pages/info/new.html',
      authRequired: false
    },
    'app.info.edit': {
      url: '/edit/:id',
      controller: 'InfoCtrl',
      templateUrl: 'admin/app/views/pages/info/edit.html',
      authRequired: false
    },



/****************************************************************************************/
    'app.products': {
      abstract: true,
      url: '/products',
      template: '<div ui-view></div>',
      authRequired: false
    },
    'app.products.list': {
      url: '/list',
      controller: 'ProductsCtrl',
      templateUrl: 'admin/app/views/pages/products/list.html',
      authRequired: false
    },
    'app.products.new': {
      url: '/new',
      controller: 'ProductsCtrl',
      templateUrl: 'admin/app/views/pages/products/new.html',
      authRequired: false
    },
    'app.products.edit': {
      url: '/edit/:id',
      controller: 'ProductsCtrl',
      templateUrl: 'admin/app/views/pages/products/edit.html',
      authRequired: false
    },
    'app.products.show': {
      url: '/show/:id',
      controller: 'ProductsCtrl',
      templateUrl: 'admin/app/views/pages/products/show.html',
      authRequired: false
    },

    'app.orders': {
      abstract: true,
      url: '/orders',
      template: '<div ui-view></div>',
      authRequired: false
    },
    'app.orders.list': {
      url: '/list',
      controller: 'OrdersCtrl',
      templateUrl: 'admin/app/views/pages/orders/list.html',
      authRequired: false
    },
    'app.orders.show': {
      url: '/show/:id',
      controller: 'OrdersCtrl',
      templateUrl: 'admin/app/views/pages/orders/show.html',
      authRequired: false
    },

    'app.users': {
      abstract: true,
      url: '/users',
      template: '<div ui-view></div>',
      authRequired: false
    },
    'app.users.list': {
      url: '/list',
      controller: 'UsersCtrl',
      templateUrl: 'admin/app/views/pages/users/list.html',
      authRequired: false
    },
    'app.users.new': {
      url: '/new',
      controller: 'UsersCtrl',
      templateUrl: 'admin/app/views/pages/users/new.html',
      authRequired: false
    },
    'app.users.edit': {
      url: '/edit/:id',
      controller: 'UsersCtrl',
      templateUrl: 'admin/app/views/pages/users/edit.html',
      authRequired: false
    },
    'app.users.show': {
      url: '/show/:id',
      controller: 'UsersCtrl',
      templateUrl: 'admin/app/views/pages/users/show.html',
      authRequired: false
    },

    'app.profile': {
      url: '/profile',
      controller: 'ProfileCtrl',
      templateUrl: 'admin/app/views/pages/profile.html',
      authRequired: false
    },
    'app.settings': {
      url: '/settings',
      controller: 'SettingsCtrl',
      templateUrl: 'admin/app/views/pages/settings.html',
      authRequired: false
    },

    'core': {
      abstract: true,
      url: '/core',
      template: '<div ui-view></div>',
      containerClass: 'core'
    },
    'core.login': {
      url: '/login',
      controller: 'AuthCtrl',
      templateUrl: 'admin/app/views/pages/login.html',
      containerClass: 'core'
    },
    'core.forgotpass': {
      url: '/forgotpass',
      controller: 'ForgotPasswordCtrl',
      templateUrl: 'admin/app/views/pages/forgotpass.html'
    }
  });
