<!doctype html>
<html ng-app="fitLogApp" ng-controller="MainCtrl" class="no-js @{{containerClass}}">
  <head>
    <meta charset="utf-8">
    <title>EasyCake Админ Панель</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width">
    <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
    <!-- build:css(.) styles/vendor.css -->
    <!-- bower:css -->
    <link href="{{ asset('admin/app/bower_components/bootstrap/dist/css/bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('admin/app/bower_components/fontawesome/css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('admin/app/bower_components/ng-table/dist/ng-table.min.css') }}" rel="stylesheet">
    <link href="{{ asset('admin/app/bower_components/chosen_v1.4.2/chosen.css') }}" rel="stylesheet">
    <link href="{{ asset('admin/app/bower_components/chosen/chosen.css') }}" rel="stylesheet">
    <link href="{{ asset('admin/app/bower_components/angular-toastr/dist/angular-toastr.css') }}" rel="stylesheet">
    <link href="{{ asset('admin/app/bower_components/magnific-popup/dist/magnific-popup.css') }}" rel="stylesheet">
    <link href="{{ asset('admin/app/bower_components/slick-carousel/slick/slick.css') }}" rel="stylesheet">
    <link href="{{ asset('admin/app/bower_components/slick-carousel/slick/slick-theme.css') }}" rel="stylesheet">
    <link href="{{ asset('admin/app/bower_components/angular-material/angular-material.min.css') }}" rel="stylesheet">
    <link href="{{ asset('admin/app/bower_components/nvd3/build/nv.d3.css') }}" rel="stylesheet">
    <!-- endbower -->
    <!-- endbuild -->
    <!-- build:css(.tmp) styles/main.css -->
    <link href="{{ asset('admin/app/styles/main.css')}}"  rel="stylesheet">
    <!-- endbuild -->
  </head>
  <body id="@{{main.appName}}" class="@{{main.settings.navbarHeaderColor}} @{{main.settings.activeColor}} @{{containerClass}} header-fixed aside-fixed rightbar-hidden appWrapper" ng-class="{'header-fixed': main.settings.headerFixed, 'header-static': !main.settings.headerFixed, 'aside-fixed': main.settings.asideFixed, 'aside-static': !main.settings.asideFixed, 'rightbar-show': main.settings.rightbarShow, 'rightbar-hidden': !main.settings.rightbarShow}" ng-cloak>

    <!--[if lt IE 7]>
      <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->

    <!-- Application content -->
    <div id="wrap" ui-view autoscroll="false"  ng-cloak class="ng-cloak"></div>

    <!-- Page Loader -->
    <div id="pageloader" page-loader></div>

    <script src="{{ asset('admin/app/bower_components/jquery/dist/jquery.js') }}"></script>
    <script src="{{ asset('admin/app/bower_components/angular/angular.js') }}"></script>
        <script src="{{ asset('admin/app/bower_components/d3/d3.min.js') }}"></script>
    <script src="{{ asset('admin/app/bower_components/nvd3/build/nv.d3.min.js') }}"></script>
    <script src="{{ asset('admin/app/bower_components/angular-nvd3/dist/angular-nvd3.min.js') }}"></script>
    
    <script src="{{ asset('admin/app/bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('admin/app/bower_components/angular-resource/angular-resource.js') }}"></script>
    <script src="{{ asset('admin/app/bower_components/angular-sanitize/angular-sanitize.js') }}"></script>
    <script src="{{ asset('https://code.angularjs.org/1.3.2/angular-animate.js') }}"></script>
    <script src="{{ asset('admin/app/bower_components/angular-touch/angular-touch.js') }}"></script>
    <script src="{{ asset('admin/app/bower_components/angular-route/angular-route.js') }}"></script>
    <script src="{{ asset('admin/app/bower_components/angular-material/angular-material.min.js') }}"></script>
    <script src="{{ asset('admin/app/bower_components/angular-aria/angular-aria.min.js') }}"></script>
    <script src="{{ asset('admin/app/bower_components/firebase/firebase.js') }}"></script>
    <script src="{{ asset('admin/app/bower_components/angularfire/dist/angularfire.js') }}"></script>
    <script src="{{ asset('admin/app/bower_components/angular-ui-router/release/angular-ui-router.js') }}"></script>
    <script src="{{ asset('admin/app/bower_components/angular-bootstrap/ui-bootstrap-tpls.js') }}"></script>
    <script src="{{ asset('admin/app/bower_components/angular-fullscreen/src/angular-fullscreen.js') }}"></script>
    <script src="{{ asset('admin/app/bower_components/ng-table/dist/ng-table.min.js') }}"></script>
    <script src="{{ asset('admin/app/bower_components/jquery.slimscroll/jquery.slimscroll.js') }}"></script>
    <script src="{{ asset('admin/app/bower_components/angular-ui-scroll/dist/ui-scroll.js') }}"></script>
    <script src="{{ asset('admin/app/bower_components/angular-ui-scrollpoint/dist/scrollpoint.js') }}"></script>
    <script src="{{ asset('admin/app/bower_components/angular-ui-event/dist/event.js') }}"></script>
    <script src="{{ asset('admin/app/bower_components/angular-ui-mask/dist/mask.js') }}"></script>
    <script src="{{ asset('admin/app/bower_components/angular-ui-validate/dist/validate.js') }}"></script>
    <script src="{{ asset('admin/app/bower_components/angular-ui-indeterminate/dist/indeterminate.js') }}"></script>
    <script src="{{ asset('admin/app/bower_components/angular-ui-uploader/dist/uploader.js') }}"></script>
    <script src="{{ asset('admin/app/bower_components/angular-ui-utils/index.js') }}"></script>
    <script src="{{ asset('admin/app/bower_components/chosen_v1.4.2/chosen.jquery.min.js') }}"></script>
    <script src="{{ asset('admin/app/bower_components/chosen/chosen.jquery.js') }}"></script>
    <script src="{{ asset('admin/app/bower_components/angular-chosen-localytics/dist/angular-chosen.js') }}"></script>
    <script src="{{ asset('admin/app/bower_components/angular-toastr/dist/angular-toastr.tpls.js') }}"></script>
    <script src="{{ asset('admin/app/bower_components/ng-file-upload/ng-file-upload.js') }}"></script>
    <script src="{{ asset('admin/app/bower_components/momentjs/moment.js') }}"></script>
    <script src="{{ asset('admin/app/bower_components/magnific-popup/dist/jquery.magnific-popup.js') }}"></script>
    <script src="{{ asset('admin/app/bower_components/slick-carousel/slick/slick.min.js') }}"></script>
    <script src="{{ asset('admin/app/bower_components/angular-slick/dist/slick.js') }}"></script>
    <script src="{{ asset('admin/app/bower_components/Chart.js/Chart.js') }}"></script>
    <!-- endbower -->
    <!-- endbuild -->

        <!-- build:js({.tmp,app}) admin/app/scripts/admin/app/scripts.js -->
        <script src="{{ asset('admin/app/scripts/app.js') }}"></script>
        <script src="{{ asset('admin/app/scripts/routes.js') }}"></script>
        <script src="{{ asset('admin/app/scripts/config.js') }}"></script>
        <script src="{{ asset('admin/app/scripts/security.js') }}"></script>
        <script src="{{ asset('admin/app/scripts/angular-locale_ru-ru.js') }}"></script>

        <script src="{{ asset('admin/app/scripts/controllers/nav.js') }}"></script>
        <script src="{{ asset('admin/app/scripts/controllers/dashboard.js') }}"></script>
        <script src="{{ asset('admin/app/scripts/controllers/auth.js') }}"></script>
        <script src="{{ asset('admin/app/scripts/controllers/categories.js') }}"></script>
        <script src="{{ asset('admin/app/scripts/controllers/shortcakes.js') }}"></script>
        <script src="{{ asset('admin/app/scripts/controllers/creams.js') }}"></script>
        <script src="{{ asset('admin/app/scripts/controllers/products.js') }}"></script>
        <script src="{{ asset('admin/app/scripts/controllers/comments.js') }}"></script>
        <script src="{{ asset('admin/app/scripts/controllers/info.js') }}"></script>
        <script src="{{ asset('admin/app/scripts/controllers/orders.js') }}"></script>
        <script src="{{ asset('admin/app/scripts/controllers/users.js') }}"></script>
        <script src="{{ asset('admin/app/scripts/controllers/profile.js') }}"></script>
        <script src="{{ asset('admin/app/scripts/controllers/settings.js') }}"></script>
        <script src="{{ asset('admin/app/scripts/controllers/forgot-password.js') }}"></script>

        <script src="{{ asset('admin/app/scripts/services/ngcloak-decorator.js') }}"></script>
        <script src="{{ asset('admin/app/scripts/services/fbutils.js') }}"></script>
        <script src="{{ asset('admin/app/scripts/services/auth.js') }}"></script>
        <script src="{{ asset('admin/app/scripts/services/digits.js') }}"></script>
        <script src="{{ asset('admin/app/scripts/services/s3upload.js') }}"></script>

        <script src="{{ asset('admin/app/scripts/directives/navcollapse.js') }}"></script>
        <script src="{{ asset('admin/app/scripts/directives/ripple.js') }}"></script>
        <script src="{{ asset('admin/app/scripts/directives/collapsesidebar.js') }}"></script>
        <script src="{{ asset('admin/app/scripts/directives/tilecontrolclose.js') }}"></script>
        <script src="{{ asset('admin/app/scripts/directives/tilecontrolfullscreen.js') }}"></script>
        <script src="{{ asset('admin/app/scripts/directives/tilecontrolrefresh.js') }}"></script>
        <script src="{{ asset('admin/app/scripts/directives/tilecontroltoggle.js') }}"></script>
        <script src="{{ asset('admin/app/scripts/directives/slimscroll.js') }}"></script>
        <script src="{{ asset('admin/app/scripts/directives/count-to.js') }}"></script>
        <script src="{{ asset('admin/app/scripts/directives/gallery.js') }}"></script>
        <script src="{{ asset('admin/app/scripts/directives/angles.js') }}"></script>

        <!-- endbuild -->
</body>
</html>
