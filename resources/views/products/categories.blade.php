@extends('layouts.app-ang')
@extends('layouts.sideMenu')
@section ('content')
<div class="container-fluid">
          <button class="pull-left btn btn-primary waves-effect waves-purple back-button" ng-click="pageBack()" ng-disabled="firstPage()">
                    Назад
          </button>
          <button class="pull-right btn btn-primary waves-effect waves-purple next-button" ng-click="pageForward()" ng-disabled="lastPage()">
               Вперед
          </button>
          
     <div class="row" ng-init="paginationInit(categoryItems)">
          <div class="col-lg-10 col-lg-offset-1 col-md-10 categories-panel header-margin" ng-init="showCategItems({{json_encode($categories)}})">

                    <form class="form-horizontal center-block search" role="form">
                     
                    <div class="sort">
                         <p>Сортировка: </p>
                         <a href="#"  ng-click="sortType = 'title'; sortReverse = !sortReverse">
                              <button type="button" class="btn btn-secondary-outline waves-effect">по имени
                              <span class="fa fa-caret-down" ng-show="sortType == 'price' && !sortReverse">
                              </span>
                              <span class="fa fa-caret-up" ng-show="sortType == 'price' && sortReverse">
                              </span>
                              </button>
                         </a>
                         <a href="#"  ng-click="sortType = 'price'; sortReverse = !sortReverse">
                              <button type="button" class="btn btn-secondary-outline waves-effect">по цене
                              <span class="fa fa-caret-down" ng-show="sortType == 'price' && !sortReverse">
                              </span>
                              <span class="fa fa-caret-up" ng-show="sortType == 'price' && sortReverse">
                              </span>
                              </button>
                         </a>
                         <div id="cd-nav">
                               <a href="#0" class="cd-nav-trigger"><span></span></a>
                              <nav id="cd-main-nav">
                              <ul>
                                   <h5>Крема</h5>
                                   <li ng-repeat="cream in creams"><input type="checkbox" ng-click="creamSort(cream.cream)"><span>@{{cream.cream}}</span></li>
                                   <h5>Коржи</h5>
                                   <li ng-repeat="shortcake in shortcakes"><input type="checkbox" ng-click="shortcakeSort(shortcake.shortcake)"><span>@{{shortcake.shortcake}}</span></li>
                              </ul>
                              <button type="button" ng-click="cr = ''; sh = ''" class="btn btn-secondary-outline waves-effect">Сбросить фильтры
                              </button>
                         </nav>
                         </div>
                         </br>
                    </div>
               </form>
                    <div id="wrap">
                         <form action="" autocomplete="on">
                              <input id="search" name="search" type="text" ng-model="search" placeholder="Что ищём?"><input id="search_submit" value="Rechercher" type="submit">
                         </form>
                    </div>
               <h3 class="text-center">
                    {{$categories}}
               </h3>
               <!-- | filter:search   -->
               <div class="col-md-4" ng-repeat="categoryItem in filtered=(categoryItems | filter:{title: search, cream:cr, shortcake:sh} | orderBy:sortType:sortReverse  |startFrom: startingItem() | limitTo: itemsPerPage ) ">
                    <div class="col-md-8 col-lg-offset-2 theme-1 hoverable z-depth-5 category-item">
                         <div class="view overlay hm-zoom ">
                              <div class="category-image">
                                   <img alt="" class="waves-effect waves-purple" src="@{{categoryItem.photo}}" title="">
                                        <br>
                                             <div class="mask waves-effect waves-purple">
                                             </div>
                                        </br>
                                   </img>
                              </div>
                         </div>
                         <h5 class="category-name text-center">
                              <a href="/торты/@{{cat}}/@{{categoryItem.title}}">
                                   @{{categoryItem.title}}
                              </a>
                         </h5>
                         <p class="category-price text-center">
                              Цена: @{{categoryItem.price}} грн.
                         </p>
                         <p class="category-price text-center">
                              Крем: @{{categoryItem.cream}}
                         </p>
                         <p class="category-price text-center">
                              Корж: @{{categoryItem.shortcake}}
                         </p>
                         @if(Auth::guest())
                         <form class="form-horizontal center-block" role="form">
                              <input name="_token" type="hidden" value="<?php echo csrf_token(); ?>">
                                   <button class="btn btn-primary btn-md categories-button center-block " ng-click="showAlert($event)">
                                        В корзину
                                   </button>
                              </input>
                         </form>
                         @else
                         <form class="form-horizontal center-block" role="form">
                              <input name="_token" type="hidden" value="<?php echo csrf_token(); ?>">
                                   <button class="btn btn-primary btn-md categories-button center-block " ng-click="addTooCart(categoryItem.id)">
                                        В корзину
                                   </button>
                              </input>
                         </form>
                         @endif
                    </div>
               </div>
          </div>
     </div>

     <div class="row" id="pagination">

          <span>
               @{{currentPage+1}} из @{{numberOfPages() }}
          </span>

     </div>
</div>
<script src="{{ asset('js/angularjs/angular.js') }}">
</script>
<script src="{{ asset('js/angularjs/angular-animate.min.js') }}">
</script>
<script src="{{ asset('js/angularjs/angular-aria.min.js') }}">
</script>
<script src="{{ asset('js/angularjs/angular-messages.js') }}">
</script>
<script src="{{ asset('js/angularjs/angular-material.min.js') }}">
</script>
<script src="{{ asset('js/angularjs/angular-locale_ru-ru.js') }}">
</script>
<script src="{{ asset('js/angular-notification-icons.min.js') }}">
</script>
@endsection
