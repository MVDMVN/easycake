@extends('layouts.app-ang')
@section ('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-10 col-lg-offset-1 col-md-10 " >
            <div class="panel panel-default confirm-margin item-panel hoverable z-depth-5">
                <div class="panel-body center-block">
                    <h2 class="text-center confirm-name">Ваш комментарий отправлен на проверку</h2>
                    <p class="text-center">Как только Ваш комментарий будет проверерн администратором, он появится на странице с товаром.</p>
                    <button class="btn btn-primary waves-effect waves-purple confirm-button">
                        <a href="{{ route('home') }}"><i class="fa fa-home" aria-hidden="true"></i> На главную</a>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
