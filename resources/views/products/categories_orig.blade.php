@extends('layouts.app')
@extends('layouts.sideMenu')
@section ('content')
<div class="container-fluid" ng-controller="cartController">
  <div class="row">
    <div class="col-lg-10 col-lg-offset-1 col-md-10  categories-panel header-margin" >
          <h3 class="text-center">{{$categories}}</h3>
          @foreach($productsCategories as $productsCategory)
        <div class="col-md-4 ">
          <div class="col-md-8 col-lg-offset-2 theme-1 hoverable z-depth-5 category-item">
            <div class="view overlay hm-zoom ">
                    <div class="category-image">
                        <img class="waves-effect waves-purple" src="{{$productsCategory->photo}}" title="" alt=""><br>
                        <div class="mask waves-effect waves-purple"></div>
                    </div>
            </div>
            <h4 class="category-name text-center"><a href="{{route('itemsPages', [$categories, $productsCategory->title])}}">{{$productsCategory->title}}</a></h4>
            <p class="category-price text-center">Цена: {{$productsCategory->price}} грн. </p>
            <form class="form-horizontal center-block" role="form">
              <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
              <button class="btn btn-primary btn-md categories-button center-block " ng-click="addTooCart({{$productsCategory->id}})">В корзину</button>



            </form>


          </div>
          </div>
          @endforeach
    </div>
  </div>
</div>
@endsection
