@extends('layouts.app-ang')
@extends('layouts.sideMenu')
@section ('content')
<div class="container-fluid">
  <div class="row">
    <div class="col-lg-8 col-lg-offset-2 col-md-10 " >
      <h1 class="text-center item-header">{{$products[0]->title}}</h1>
      <div class="panel panel-default item-panel hoverable z-depth-5">
        <div class="panel-body center-block">
          <br>
          <div class="col-lg-6">
            <div class="view overlay hm-zoom ">
              <div class="category-image">
                <img src="{{ asset('some/cake_1.jpg') }}" class="center-block "  alt="" >
                <div class="mask waves-effect waves-purple"></div>
              </div>
            </div>
          </div>
          <div class="col-lg-6 info ">
            <h4 class="text-center">{{$products[0]->title}}</h4>
            <p class=" text-center">{{$products[0]->description}}</p>
            <p class=" text-center">Крем: {{$cream[0]->cream}}</p>
            <p class=" text-center">Корж: {{$shortcake[0]->shortcake}}</p>
            <p class=" text-center">Вес: {{$products[0]->weight}} кг.</p>
            <p class=" text-center">Цена: {{$products[0]->price}} грн.</p>
            <div class="col-md-4 col-md-offset-2">
              @if(Auth::guest())
              <button type="submit" class="btn btn-primary waves-effect waves-purple center-block" ng-click="showAlert($event)">
              <a href="#"><i class="fa fa-cart-arrow-down"></i>В корзину</a>
              </button>
              @else
              <button type="submit" class="btn btn-primary waves-effect waves-purple center-block" ng-click="addTooCart({{$products[0]->id}})">
              <a href="#"><i class="fa fa-cart-arrow-down"></i>В корзину</a>
              </button>
              @endif
            </div>
            <div class="col-md-6 rating">
              Рейтинг товара: {{$rating[0]->rating}}
            </div>
            @if (Auth::guest())
            <input-stars ng-model="Rt.rating" max="5" readonly="true"></input-stars> Звёзд: @{{Rt.rating}}
            @else
            <div class="col-md-6" ng-init="idUser = {{Auth::user()->id}}" ng-controller="RatingCtrl as Rt">
              <input-stars ng-model="Rt.rating" max="5" ng-click ="setRating(Rt.rating, {{Auth::user()->id}} ,{{$products[0]->id}})" "></input-stars>
              
            </div>
            @endif
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid" >
  <div class="row">
    <div class="col-lg-8 col-lg-offset-2">
      <h4 class="comment-header">Комментарии</h4>
      @foreach($comments as $comment)
      <div class="comment-wrap ">
        <div class="photo">
          {{$comment->usrName}}
        </div>
        <div class="comment-block">
          <p class="comment-text">{{$comment->comment}}</p>
          <div class="bottom-comment">
            <div class="comment-date">{{$comment->created_at}}</div>
          </div>
        </div>
      </div>
      @endforeach
    </div>
  </div>
</div>
<div class="container-fluid" >
  <div class="row">
    <div class="col-lg-8 col-lg-offset-2 col-md-10">
      <div class="panel panel-default history  hoverable z-depth-5">
        {{--<div class="panel-heading text-center header-text">--}}
          {{--Оставьте свой комментарий--}}
        {{--</div>--}}
        <div class="panel-body center-block">
          @if(Auth::guest())
          <form role="form" method="POST">
            {!! csrf_field() !!}
            <div class="col-md-12 ">
              <h4 class="comment-header">Оставить комментарий</h4>
              <md-input-container class="md-block">
              <label>Введите Ваш комментарий</label>
              <textarea ng-model="user.biography" md-maxlength="150" rows="5" md-select-on-focus=""></textarea>
              </md-input-container>
              <button class="btn btn-primary waves-effect waves-purple center-block" ng-click="commentAlert($event)">
            <i class="fa fa-comment" aria-hidden="true"></i>Отправить</a>
            </button>
          </form>
          @else
          <form role="form" method="POST" action="{{ route('comment') }}">
            {!! csrf_field() !!}
            <div class="col-md-12 ">
              <h4 class="comment-header">Оставить комментарий</h4>
              <md-input-container class="md-block">
              <label>Введите Ваш комментарий</label>
              <textarea name="comment" md-maxlength="150" rows="5" md-select-on-focus=""></textarea>
              </md-input-container>
              <input type="hidden" name="product" value="{{$products[0]->id}}">
              <button type="submit" class="btn btn-primary waves-effect waves-purple center-block" ng-click="commentAlert($event)">
            <i class="fa fa-comment" aria-hidden="true"></i>Отправить</a>
            </button>
          </form>
          @endif
        </div>
      </div>
    </div>
  </div>
</div>
</div>
<script src="{{ asset('js/angularjs/angular.js') }}"></script>
<script src="{{ asset('js/angularjs/angular-animate.min.js') }}"></script>
<script src="{{ asset('js/angularjs/angular-aria.min.js') }}"></script>
<script src="{{ asset('js/angularjs/angular-messages.js') }}"></script>
<script src="{{ asset('js/angularjs/angular-material.min.js') }}"></script>
<script src="{{ asset('js/angularjs/angular-locale_ru-ru.js') }}"></script>
<script src="{{ asset('js/angular-input-stars-directive/angular-input-stars.js') }}"></script>
@endsection