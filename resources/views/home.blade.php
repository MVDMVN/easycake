@extends('layouts.app')
@extends('layouts.footer')
@section('content')
<div class="conteiner-fluid">
    <h2 class="text-center slick-header">Категории тортиков</h2>
    <div class="container-fluid slick-carousel" ng-controller="homeController">
    <slick settings="slickConfig">
                @foreach( $sideMenus as $sideMenu)
                    <div class="col-md-3 ">
                        <div class="col-md-10 col-md-offset-1">
                            <div class="theme-1 hoverable z-depth-2">
                                <div class="view overlay hm-zoom ">
                                    <div class="home-category-image">
                                        <a href="{{route('category', $sideMenu->title )}}"><img width="200" height="150" src="{{$sideMenu->photo}}" alt="{{$sideMenu->title}}" class="center-block " ></a>
                                    </div>
                                </div>
                                <h6 class="text-center theme-item-header"><a href="{{route('category', $sideMenu->title )}}">{{$sideMenu->title}}</a></h6>
                                <a href="{{route('category', $sideMenu->title )}}"><button class="btn btn-primary btn-md cake-button center-block waves-effect waves-purple">Показать</button></a>
                            </div>
                        </div>
                    </div>
                @endforeach
    </slick>
   </div>

    <div class="container-fluid" class="top">
        <!-- Items section Start -->
        <section class="cake-theme">
            <h2 class="text-center theme-headers">Топ продаж</h2>
            <div class="row">
                @foreach($topSellsItems as $topSellItem)
                    <div class="col-md-3  ">
                        <div class="col-md-10 col-md-offset-1">
                            <div class="theme-1 hoverable z-depth-5">
                                <div class="view overlay hm-zoom ">
                                    <div class="home-category-image">
                                        <a href="/торты/{{$topSellItem[0]->ctID}}/{{$topSellItem[0]->title}}"><img src="{{ $topSellItem[0]->photo }}" class="center-block "  alt="" ></a>
                                    </div>
                                </div>
                              <h6 class="text-center theme-item-header"><a href="/торты/{{$topSellItem[0]->ctID}}/{{$topSellItem[0]->title}}">{{$topSellItem[0]->title}}</a></h6>
                                <p class="text-center theme-item-desc">{{$topSellItem[0]->description}}</p>
                                <a href="/торты/{{$topSellItem[0]->ctID}}/{{$topSellItem[0]->title}}"><button class="btn btn-primary btn-md cake-button center-block waves-effect waves-purple">Подробнее</button></a>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            <section id="section01" class="demo">
                <a href="#items" du-smooth-scroll du-scrollspy><span></span>Спуститься вниз</a>
            </section>
        </section>
    </div>
    <!-- Items section End -->




    <!-- Parallax section Start -->
    <section class="parallax" id="section-1">
        {{--<h3 class="parallax-text">Самые вкусные торты у нас!</h3>--}}
    </section>
    <!-- Parallax section End-->


    <!-- Top Items section Start -->
    <div id="items" class="container-fluid">
        <!-- Items section Start -->
        <section class="cake-theme">
            <h2 class="text-center theme-headers">Новые тортики</h2>
            <div class="row">
                @foreach ($newItems as $newItem)
                    <div class="col-md-3  ">
                        <div class="col-md-10 col-md-offset-1">
                            <div class="theme-1 hoverable z-depth-5">
                                <div class="view overlay hm-zoom ">
                                    <div class="category-image">
                                        <a href="/торты/{{$newItem->id_category}}/{{$newItem->title}}"><img src="{{ asset($newItem->photo) }}" class="center-block "  alt="" ></a>
                                        <div class="mask waves-effect waves-purple"></div>
                                    </div>
                                </div>
                                <h6 class="text-center theme-item-header"><a href="/торты/{{$newItem->id_category}}/{{$newItem->title}}">{{$newItem->title}}<a href="/торты/{{$newItem->id_category}}/{{$newItem->title}}"></a></h6>
                                <p class="text-center theme-item-desc">{{$newItem->description}}</p>
                                <a href="/торты/{{$newItem->id_category}}/{{$newItem->title}}"><button class="btn btn-primary btn-md cake-button center-block waves-effect waves-purple">Подробнее</button></a>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </section>
    </div>
    <!-- Top Items section End -->

    <!-- Parallax section Start -->
    <section class="parallax" id="section-2">
        <!-- Social section Start -->
        <section class="social verticalcenter center-block   col-lg-12 ">
            <!-- <h2 class="text-center theme-headers">Тематики тортов</h2> -->
            <div class="social-item col-md-3">
                <div class="col-md-6 col-md-offset-3">
                    <img class="img-responsive " src="{{ asset('images/social/facebook.svg') }}" alt="facebook" title="facebook" />
                    <div class="mask waves-effect waves-purple"></div>
                </div>
            </div>
            <div class="social-item col-md-3">
                <div class="col-md-6 col-md-offset-3">
                    <img class="img-responsive" src="{{ asset('images/social/twitter.svg') }}" alt="twitter" title="twitter" />
                    <div class="mask waves-effect waves-purple"></div>
                </div>
            </div>
            <div class="social-item col-md-3">
                <div class="col-md-6 col-md-offset-3">
                    <img class="img-responsive" src="{{ asset('images/social/instagram.svg') }}" alt="instagram" title="instagram" />
                    <div class="mask waves-effect waves-purple"></div>
                </div>
            </div>
            <div class="social-item col-md-3">
                <div class="col-md-6 col-md-offset-3">
                    <img class="img-responsive" src="{{ asset('images/social/google.svg') }}" alt="google" title="google" >
                    <div class="mask waves-effect waves-purple"></div>
                </div>
            </div>
        </section>
        <!-- Social section End-->
    </section>
    <!-- Parallax section End-->

    <!-- Last comments section Start -->
    <div class="container-fluid">
        <!-- Items section Start -->
        <section class="cake-theme">
            <h2 class="text-center theme-headers">Последние комментарии</h2>
            <div class="row">
                @foreach ($lastComments as $lastComment)
                    <div class="col-md-3  ">
                        <div class="col-md-10 col-md-offset-1">
                            <div class="theme-1 hoverable z-depth-5">
                                <div class="view overlay hm-zoom ">
                                    <div class="category-image">
                                        <a href="/торты/{{$lastComment->id_category}}/{{$lastComment->prTitle}}"><img src="{{ asset($lastComment->photo) }}" class="center-block "  alt="" ></a>
                                        <div class="mask waves-effect waves-purple"></div>
                                    </div>
                                </div>
                                <h6 class="text-center theme-item-header"><a href="/торты/{{$lastComment->id_category}}/{{$lastComment->prTitle}}">{{$lastComment->prTitle}}</a></h6>
                                <p class="text-center theme-item-desc">{{$lastComment->comment}}</p>
                                <p class="text-center theme-item-desc">{{$lastComment->usrName}}</p>
                                <p class="text-center theme-item-desc">{{$lastComment->created_at}}</p>
                                <a href="/торты/{{$lastComment->id_category}}/{{$lastComment->prTitle}}"><button class="btn btn-primary btn-md cake-button center-block waves-effect waves-purple">Подробнее</button></a>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </section>
    </div>
    <!-- Last comments section End -->

    <!-- Map section Start -->
    <footer>
    <div class="col-lg-9">
        <div id="map-container" class="card-panel hoverable wow fadeInUp hoverable z-depth-5" style="height: 400px" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d686.7982775198279!2d30.746124315471604!3d46.48448849553497!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0000000000000000%3A0x0615d78617e79394!2z0KHQtdGA0LLQtdGAINCe0LTQtdGB0YHQutC40Lkg0JrQvtC70LvQtdC00LYg0JrQvtC80L_RjNGO0YLQtdGA0L3Ri9GFINCi0LXRhdC90L7Qu9C-0LPQuNC5!5e0!3m2!1sru!2sru!4v1457272198724"  frameborder="0" style="border:0" allowfullscreen>
        </div>
    </div>
    <div class="col-lg-3 contacts ">
        <div class="panel panel-default contact-panel fadeInUp hoverable z-depth-5">
            <div class="panel-heading text-center header-text">Контакты</div>
            <div class="panel-body">

                    <ul>
                        <li><i class="fa fa-map-marker"></i><span>г. Одесса ул. Польский спуск, 2</span></li>
                        <li><i class="material-icons">phone</i><span>+380934830166</span> </li>
                        <li><i class="material-icons">phone</i><span>+380937516158</span></li>
                        <li><i class="material-icons">email</i><span>easycake@cake.com </span></li>
                    </ul>

            </div>
        </div>
    </div>
    </footer>
    <div class="margin-bottom"></div>
    <!-- Map section End -->
 </div>   
@endsection