@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Моё тестовое приложение</div>
                    <div class="panel-body">
                      <table>
                    @foreach($envs as $env)
                      <tr>
                          <td>{{$env->name}} - {{$env->created_at}}</td>
                      </tr>
                    @endforeach
                  </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
