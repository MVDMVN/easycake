@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-6 col-md-offset-3 login-panel header-margin">
            <div class="panel panel-default hoverable z-depth-5">
                <div class="panel-heading center-text header-text">Сброс пароля</div>

                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/password/reset') }}">
                        {!! csrf_field() !!}
                        <div class="col-md-6 col-md-offset-3">
                 <div class=" form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <span class="input input--nao">
                        <input class="input__field input__field--nao" name="email"  type="email" id="input-1" value="{{ $email or old('email') }}" />
                        <label class="input__label input__label--nao" for="input-1">
                            <span class="input__label-content input__label-content--nao">E-mail</span>
                        </label>

                        <svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
                            <path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
                        </svg>
                    </span>
                    </div>
                    @if ($errors->has('email'))
                    <p class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </p>
                    @endif

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <span class="input input--nao">
                                <input class="input__field input__field--nao" name="password" type="password" id="input-2" />
                                <label class="input__label input__label--nao" for="input-2">
                                    <span class="input__label-content input__label-content--nao">Новый пароль*</span>
                                </label>
                                <svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
                                    <path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
                                </svg>
                            </span>
                        </div>
                        @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                        @endif
                        <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                            <span class="input input--nao">
                                <input class="input__field input__field--nao" name="password_confirmation" type="password" id="input-2" />
                                <label class="input__label input__label--nao" for="input-2">
                                    <span class="input__label-content input__label-content--nao">Подтвердите пароль*</span>
                                </label>
                                <svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
                                    <path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
                                </svg>
                            </span>
                        </div>
                        @if ($errors->has('password_confirmation'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password_confirmation') }}</strong>
                        </span>
                        @endif
</div>
                        <div class="form-group center-block reset">
                            <div class="col-md-4 col-md-offset-4">
                                <button type="submit" class="btn btn-primary btn-ok waves-effect waves-purple center-block">
                                    <i class="fa fa-btn fa-refresh"></i>Сбросить пароль
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
