@extends('layouts.app')

<!-- Main Content -->
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-6 col-md-offset-3 header-margin login-panel">
            <div class="panel panel-default hoverable z-depth-5">
                <div class="panel-heading center-text header-text">Сброс пароля</div>
                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/password/email') }}">
                        {!! csrf_field() !!}
                        <div class="col-md-8 col-md-offset-2">
                <div class=" form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <span class="input input--nao">
                        <input class="input__field input__field--nao" name="email"  type="email" id="input-1" />
                        <label class="input__label input__label--nao" for="input-1">
                            <span class="input__label-content input__label-content--nao">E-mail*</span>
                        </label>

                        <svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
                            <path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
                        </svg>
                    </span>
                    </div>
                    @if ($errors->has('email'))
                    <p class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </p>
                    @endif
                        </div>
                        <div class="form-group">
                            <div class="col-md-offset-2">
                                <button type="submit" class="btn btn-primary btn-ok waves-effect waves-purple center-block">
                                    <i class="fa fa-btn fa-envelope"></i>Отправить сообщение на почтовый ящик
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
