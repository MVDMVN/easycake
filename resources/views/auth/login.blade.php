@extends('layouts.app')
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-6 col-md-offset-3 login-panel  header-margin">
            <div class="panel panel-default hoverable z-depth-5">
                <div class="panel-heading text-center header-text">Авторизация</div>
                 <div class="panel-body">
           <form class="form-horizontal " role="form" method="POST" action="{{ url('/login') }}"> {!! csrf_field() !!}
             <div class="col-md-4 col-md-offset-4">
                 <div class=" form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <span class="input input--nao">
                        <input class="input__field input__field--nao" name="email"  type="email" id="input-1" />
                        <label class="input__label input__label--nao" for="input-1">
                            <span class="input__label-content input__label-content--nao">E-mail*</span>
                        </label>

                        <svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
                            <path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
                        </svg>
                    </span>
                    </div>
                    @if ($errors->has('email'))
                    <p class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </p>
                    @endif
                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                    <span class="input input--nao">
                        <input class="input__field input__field--nao" type="password" name="password" id="input-2" />
                        <label class="input__label input__label--nao" for="input-2">
                            <span class="input__label-content input__label-content--nao">Пароль*</span>
                        </label>
                        <svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
                            <path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
                        </svg>
                    </span>
                    </div>
                                    @if ($errors->has('password'))
                <span class="help-block">
                    <strong>{{ $errors->first('password') }}</strong>
                </span>
                @endif
                </div>
                <div class="form-group">
                    <div class="col-md-4 col-md-offset-4 remember">
                        <div class="checkbox">
                            <p>Запомнить меня</p>
                            <label>
                                <!-- <input type="checkbox" id="test6" checked="checked" name="remember"> Запомнить меня -->
                                <input type="checkbox" id="test1" /><label for="test1"><span class="ui"></span></label>
                            </label>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                        <a class="btn btn-primary btn-reset" href="{{ url('/password/reset') }}">Забыли пароль?</a>
                        <button type="submit" class="btn btn-primary btn-ok "><i class="fa fa-btn fa-sign-in"></i>Войти</button>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
