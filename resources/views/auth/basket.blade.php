@extends('layouts.app-ang')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-10 col-md-offset-1">
                <div class="panel panel-default history login-panel header-margin hoverable z-depth-5">
                    <div class="panel-heading text-center header-text" >
                        Ваша корзина заказов, {{Auth::user()->name}}
                    </div>
                    <div class="panel-body center-block">
                        <table class="table " ng-hide="basketEmpty">
                            <thead>
                            <th>
                                Название
                            </th>
                            <th>
                                Фото
                            </th>
                            <th>
                                Цена
                            </th>
                            <th>
                                Количество
                            </th>
                            <th>
                                Сумма
                            </th>
                            <th>
                                Удалить
                            </th>
                            </thead>
                            <tr ng-repeat="basketItem in basketItems" >
                                <td>
                                    @{{basketItem.title}}
                                </td>
                                <td>
                                    <img width="160" height="120" src="@{{basketItem.photo}}" title="@{{basketItem.title}}"/>
                                </td>
                                <td>
                                    @{{basketItem.price}} грн.
                                </td>
                                <td>
                                 <md-slider-container>
                                    <md-slider flex="" ng-model="basketItem.qty" min="1" max="15" aria-label="green" id="green-slider" ng-change="qtyUpdate(basketItem.id, basketItem.qty)" class="md-accent"></md-slider>
                                   <md-input-container>
                                    <input flex="" type="number" ng-model="basketItem.qty" ng-change="qtyUpdate(basketItem.id, basketItem.qty)" aria-label="qty" aria-controls="green-slider">
                                   </md-input-container>
                                 </md-slider-container>
                                </td>
                                <td>
                                <section id="section01" class="demo">
                                    <a href="#orderForm" du-smooth-scroll du-scrollspy><span></span>Перейти к заказу</a>
                                </section>
                                </td>
                                <td >
                                    @{{basketItem.price*basketItem.qty;}} грн.
                                </td>
                                <td>
                                    <a ng-click="removeFromBasket(basketItem.id)">
                                        <i class="material-icons">
                                            close
                                        </i>
                                    </a>
                                </td>
                            </tr>
                        </table>
                        <hr ng-hide="basketEmpty">
                        <p class="basket-summary" ng-hide="basketEmpty">
                            Сумма вашего заказа: <span id="sum"> @{{Summary}} грн. </span>
                        </p>
                            <div class="row"  ng-hide="!basketEmpty">
                                <div class="col-lg-6 col-lg-offset-3 col-md-10 col-md-offset-1">
                                    <div class="panel panel-default empty-panel hoverable z-depth-5">
                                        <div class="panel-heading text-center header-text">Ваша корзина пуста!</div>
                                    </div>
                                </div>
                            </div>
                    </div>
                </div>
            </div>
            <div id="orderForm" class="col-lg-10 col-md-offset-1" ng-hide="basketEmpty">
                <div class="panel panel-default history login-panel header-margin hoverable z-depth-5">
                    <div class="panel-heading text-center header-text">
                        Введите ваши данные 
                    </div>
                    <div class="panel-body center-block">
                        <div class="orderForm">
                            <form role="form">
                                {!! csrf_field() !!}
                                <div class="col-md-6 col-md-offset-3">
                                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                             <span class="input input--nao">
                                                  <input class="input__field input__field--nao " disabled="" id="input-1" name="name" type="text" "/>
                                                  <label class="input__label input__label--nao" for="input-1">
                                                       <span class="input__label-content input__label-content--nao">
                                                            Имя
                                                       </span>
                                                  </label>
                                                  <svg class="graphic graphic--nao" height="100%" preserveaspectratio="none" viewbox="0 0 1200 60" width="300%">
                                                       <path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0">
                                                       </path>
                                                  </svg>
                                             </span>
                                    </div>
                                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                             <span class="input input--nao">
                                                  <input class="input__field input__field--nao" disabled="" id="input-2" name="email" type="text"/>
                                                  <label class="input__label input__label--nao" for="input-2">
                                                       <span class="input__label-content input__label-content--nao">
                                                            Email
                                                       </span>
                                                  </label>
                                                  <svg class="graphic graphic--nao" height="100%" preserveaspectratio="none" viewbox="0 0 1200 60" width="300%">
                                                       <path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0">
                                                       </path>
                                                  </svg>
                                             </span>
                                    </div>
                                    <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                                             <span class="input input--nao">
                                                  <input class="input__field input__field--nao" disabled="" id="input-3" name="phone" type="text" />
                                                  <label class="input__label input__label--nao" for="input-2">
                                                       <span class="input__label-content input__label-content--nao">
                                                            Телефон
                                                       </span>
                                                  </label>
                                                  <svg class="graphic graphic--nao" height="100%" preserveaspectratio="none" viewbox="0 0 1200 60" width="300%">
                                                       <path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0">
                                                       </path>
                                                  </svg>
                                             </span>
                                    </div>
                                    <div class="form-group">
                                             <span class="input input--nao">
                                                  <input class="input__field input__field--nao" id="input-7" name="address" ng-model="orderForm.additionalPhone" type="text"/>
                                                  <label class="input__label input__label--nao" for="input-2">
                                                       <span class="input__label-content input__label-content--nao">
                                                            Дополнительный телефон
                                                       </span>
                                                  </label>
                                                  <svg class="graphic graphic--nao" height="100%" preserveaspectratio="none" viewbox="0 0 1200 60" width="300%">
                                                       <path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0">
                                                       </path>
                                                  </svg>
                                             </span>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-lg-4">
                                                  <span class="input input--nao">
                                                       <input class="input__field input__field--nao" id="input-4" name="address" ng-model="orderForm.street" ng-required="true" type="text"/>
                                                       <label class="input__label input__label--nao" for="input-2">
                                                            <span class="input__label-content input__label-content--nao">
                                                                 Улица
                                                            </span>
                                                       </label>
                                                       <svg class="graphic graphic--nao" height="100%" preserveaspectratio="none" viewbox="0 0 1200 60" width="300%">
                                                            <path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0">
                                                            </path>
                                                       </svg>
                                                  </span>
                                        </div>
                                        <div class="form-group col-lg-4">
                                                  <span class="input input--nao ">
                                                       <input class="input__field input__field--nao col-lg-offset-2" id="input-5" name="address" ng-model="orderForm.houseNumber" ng-required="true" type="text"/>
                                                       <label class="input__label input__label--nao" for="input-2">
                                                            <span class="input__label-content input__label-content--nao">
                                                                 Номер дома
                                                            </span>
                                                       </label>
                                                       <svg class="graphic graphic--nao" height="100%" preserveaspectratio="none" viewbox="0 0 1200 60" width="300%">
                                                            <path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0">
                                                            </path>
                                                       </svg>
                                                  </span>
                                        </div>
                                        <div class="form-group col-lg-4">
                                          <span class="input input--nao">
                                               <input class="input__field input__field--nao" id="input-6" name="address" ng-model="orderForm.apartmentNumber" type="text"/>
                                               <label class="input__label input__label--nao" for="input-2">
                                                    <span class="input__label-content input__label-content--nao">
                                                         Номер квартиры
                                                    </span>
                                               </label>
                                               <svg class="graphic graphic--nao" height="100%" preserveaspectratio="none" viewbox="0 0 1200 60" width="300%">
                                                    <path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0">
                                                    </path>
                                               </svg>
                                          </span>
                                        </div>
                                    </div>
                                    <p class="date-basket">Хочу получить торт на:</p>
                                    <md-datepicker md-placeholder="Выберите дату" ng-model="orderForm.date" md-min-date="minDate" required="true">
                                    </md-datepicker>
                                    <div class="form-group">
                                        <div class=" col-md-8 col-md-offset-2">
                                            <button class="btn btn-primary btn-ok waves-effect waves-purple center-block" ng-click="orderSend(basketItem.qty, orderForm.additionalPhone, orderForm.street, orderForm.houseNumber, orderForm.apartmentNumber, orderForm.date)" type="submit">
                                                 <i class="fa fa-btn fa-pencil-square-o">
                                                 </i>
                                                 Оформить заказ
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="{{ asset('js/angularjs/angular.js') }}">
    </script>
    <script src="{{ asset('js/angularjs/angular-animate.min.js') }}">
    </script>
    <script src="{{ asset('js/angularjs/angular-aria.min.js') }}">
    </script>
    <script src="{{ asset('js/angularjs/angular-messages.js') }}">
    </script>
    <script src="{{ asset('js/angularjs/angular-material.min.js') }}">
    </script>
    <script src="{{ asset('js/angularjs/angular-locale_ru-ru.js') }}">
    </script>
@endsection
