@extends('layouts.app-ang')
@section('content')
<div class="container-fluid">
     <div class="row">
          <div class="col-lg-10 col-md-offset-1">
               <div class="panel panel-default history login-panel header-margin hoverable z-depth-5">
                    <div class="panel-heading text-center header-text">
                         Ваша история заказов
                    </div>
                    <div class="panel-body center-block">
                         <table class="table ">
                              <thead>
                                   <th>
                                        Номер заказа
                                   </th>
                                   <th>
                                        Название
                                   </th>
                                   <th>
                                        Фото
                                   </th>
                                   <th>
                                        Цена
                                   </th>
                                   <th>
                                        Количество
                                   </th>
                                   <th>
                                        Дата оформления
                                   </th>
                              </thead>
                              @foreach($ordersHistories as $orderHistory)
                              <tr>
                                   <td>{{$orderHistory->id}}</td>
                                   <td>{{$orderHistory->title}}</td>
                                   <td><img width="140" height="100" src="{{$orderHistory->photo}}" /></td>
                                   <td>{{$orderHistory->price}}</td>
                                   <td>{{$orderHistory->count}}</td>
                                   <td>{{ date('F d, Y', strtotime($orderHistory->created_at))}}</td>
                              </tr>
                              @endforeach
                         </table>
                    </div>
               </div>
          </div>
     </div>
</div>
<script src="{{ asset('js/angularjs/angular.js') }}">
</script>
<script src="{{ asset('js/angularjs/angular-animate.min.js') }}">
</script>
<script src="{{ asset('js/angularjs/angular-aria.min.js') }}">
</script>
<script src="{{ asset('js/angularjs/angular-messages.js') }}">
</script>
<script src="{{ asset('js/angularjs/angular-material.min.js') }}">
</script>
<script src="{{ asset('js/angularjs/angular-locale_ru-ru.js') }}">
</script>
@endsection
