@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-body center-block">
                    <h3>Данные были успешно отредактированы!</h3>
                    <table class="table table-striped">
                       <tr>
                        <th>Поле</th>
                        <th>Значение</th>
                       </tr>
                       <tr>
                        <td>Имя</td>
                        <td>{{$nameUsers->name}}</td>
                      </tr>
                       <tr>
                        <td>Email</td>
                        <td>{{$nameUsers->email}}</td>
                      </tr>
                       <tr>
                        <td>Телефон</td>
                        <td>{{$nameUsers->phone}}</td>
                      </tr>
                     </table>
                     <button type="submit" class="btn btn-primary waves-effect waves-purple">
                       <a href="{{ route('home')}}"><i class="fa fa-btn fa-pencil-square-o">Вернуться на главную</i></a>
                     </button>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
