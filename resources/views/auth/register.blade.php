@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-6 col-md-offset-3 reg-panel header-margin">
            <div class="panel panel-default hoverable z-depth-5">
                <div class="panel-heading text-center header-text">Регистрация</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/register') }}">{!! csrf_field() !!}
                    <div class="col-md-8 col-md-offset-2">
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <span class="input input--nao">
                                <input class="input__field input__field--nao form-control" name="name" type="text" id="input-1" value="{{ old('name') }}" />
                                <label class="input__label input__label--nao" for="input-1">
                                    <span class="input__label-content input__label-content--nao">Имя</span>
                                </label>
                                <svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
                                    <path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
                                </svg>
                            </span>
                        </div>
                        @if ($errors->has('name'))
                        <span class="help-block">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                        @endif
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <span class="input input--nao">
                                <input class="input__field input__field--nao" type="text" name="email"  id="input-2" value="{{ old('email') }}" />
                                <label class="input__label input__label--nao" for="input-2">
                                    <span class="input__label-content input__label-content--nao">Email</span>
                                </label>
                                <svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
                                    <path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
                                </svg>
                            </span>
                        </div>
                        @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                        @endif
                        <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                            <span class="input input--nao">
                                <input class="input__field input__field--nao" name="phone" type="text" id="input-3" value="{{ old ('phone') }}"  />
                                <label class="input__label input__label--nao" for="input-3">
                                    <span class="input__label-content input__label-content--nao">Телефон</span>
                                </label>
                                <svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
                                    <path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
                                </svg>
                            </span>
                        </div>
                        @if ($errors->has('phone'))
                        <span class="help-block">
                            <strong>{{ $errors->first('phone') }}</strong>
                        </span>
                        @endif
                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <span class="input input--nao">
                                <input class="input__field input__field--nao" name="password" type="password" id="input-4" autocomplete="off" />
                                <label class="input__label input__label--nao" for="input-4">
                                    <span class="input__label-content input__label-content--nao">Пароль</span>
                                </label>
                                <svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
                                    <path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
                                </svg>
                            </span>
                        </div>
                        @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                        @endif
                        <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                            <span class="input input--nao">
                                <input class="input__field input__field--nao" name="password_confirmation" type="password" id="input-5" autocomplete="off" />
                                <label class="input__label input__label--nao" for="input-5">
                                    <span class="input__label-content input__label-content--nao">Повторите пароль</span>
                                </label>
                                <svg class="graphic graphic--nao" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
                                    <path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
                                </svg>
                            </span>
                        </div>
                        @if ($errors->has('password_confirmation'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password_confirmation') }}</strong>
                        </span>
                        @endif
                        <div class="form-group">
                        <div class=" col-lg-8 col-lg-offset-2">
                                <button type="submit" class="btn btn-primary btn-ok waves-effect waves-purple">
                                <i class="fa fa-btn fa-user"></i>Зарегистрироваться
                                </button>
                        </div>
                        </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection