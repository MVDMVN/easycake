<?php

use Illuminate\Database\Seeder;

class RatingTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        $limit = 1000;

        for ($i = 0; $i < $limit; $i++) {
            DB::table('rating')->insert([
                'id_user'                       => $faker->numberBetween($min = 1, $max = 29),

                'id_product'                    => $faker->numberBetween($min = 1, $max = 200),

                'rating'                        => $faker->numberBetween($min = 1, $max = 5),

                'created_at'                    => $faker->dateTimeBetween($startDate = '- 200 days', $endDate = 'now'),

            ]);
        }
    }
}
