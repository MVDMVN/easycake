<?php

use Illuminate\Database\Seeder;

class CommentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        $limit = 300;

        for ($i = 0; $i < $limit; $i++) {
            DB::table('comments')->insert([ //,
                'id_user'        => $faker->numberBetween($min = 2, $max = 30),
                'id_product'  => $faker->numberBetween($min = 1, $max = 200),
                'id_status' => $faker->numberBetween($min = 1, $max = 3),
                'comment' => $faker->realText($maxNbChars = 200, $indexSize = 2),

                'created_at'   => $faker->dateTimeBetween($startDate = '- 200 days', $endDate = 'now'),
            ]);
        }
    }
}
