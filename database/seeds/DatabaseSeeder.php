<?php

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder {
    public function run() {

        Model::unguard();

        $this->call(UsersTableSeeder::class);
        $this->call(CategoriesTableSeeder::class);
        $this->call(PaymentTableSeeder::class);
        $this->call(StatusesTableSeeder::class);
        $this->call(CreamsTableSeeder::class);
        $this->call(ShortcakesTableSeeder::class);
        $this->call(ProductsTableSeeder::class);
        $this->call(OrdersTableSeeder::class);
        $this->call(InfoTableSeeder::class);
        $this->call(CommentStatusesTableSeeder::class);
        $this->call(CommentsTableSeeder::class);
        $this->call(RatingTableSeeder::class);
        Model::reguard();
    }
}

// class UserTableSeeder extends Seeder {
//
//
// }
