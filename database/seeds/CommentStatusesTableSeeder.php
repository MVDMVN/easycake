<?php

use Illuminate\Database\Seeder;

class CommentStatusesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        {
            DB::table('comment_statuses')->insert([
                'status' => 'Опубликовано',
            ]);
            DB::table('comment_statuses')->insert([
                'status' => 'Отклонено',
            ]);
            DB::table('comment_statuses')->insert([
                'status' => 'Ожидаент публикации',
            ]);
        }
    }
}
