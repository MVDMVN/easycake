<?php

use Illuminate\Database\Seeder;

class ShortcakesTableSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $faker = Faker\Factory::create();
        {
            DB::table('shortcakes')->insert([
                'shortcake' => 'Шоколадный',
                'created_at' => $faker->dateTimeBetween($startDate = '- 250 days', $endDate = 'now'),
            ]);
            DB::table('shortcakes')->insert([
                'shortcake' => 'Кокосовый',
                'created_at' => $faker->dateTimeBetween($startDate = '- 250 days', $endDate = 'now'),
            ]);
            DB::table('shortcakes')->insert([
                'shortcake' => 'Ванильный',
                'created_at' => $faker->dateTimeBetween($startDate = '- 250 days', $endDate = 'now'),
            ]);
            DB::table('shortcakes')->insert([
                'shortcake' => 'Творожный',
                'created_at' => $faker->dateTimeBetween($startDate = '- 250 days', $endDate = 'now'),
            ]);
            DB::table('shortcakes')->insert([
                'shortcake' => 'Медовый',
                'created_at' => $faker->dateTimeBetween($startDate = '- 250 days', $endDate = 'now'),
            ]);
        }
    }
}
