<?php

use Illuminate\Database\Seeder;

class OrdersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        $limit = 100;

        for ($i = 0; $i < $limit; $i++) {
            $ii = $i;
            DB::table('orders')->insert([ //,
                'id'                            => ++$ii,
                'id_user'                       => $faker->numberBetween($min = 1, $max = 15),
                'id_product'                    => $faker->numberBetween($min = 1, $max = 15),
                'id_payment'                    => $faker->numberBetween($min = 1, $max = 3),
                'id_status'                     => $faker->numberBetween($min = 1, $max = 6),
                'count'                         => $faker->numberBetween($min = 1, $max = 4),
                'expected_date'                 => $faker->dateTimeBetween($startDate = '- 100 days', $endDate = '+ 30 days'),
                'shipping_address'              => $faker->streetName,
                'shipping_address_house_number' => $faker->numberBetween($min = 1, $max = 120),
                'shipping_address_room_number'  => $faker->numberBetween($min = 2, $max = 99),
                'shipping_additional_phone'     => $faker->phoneNumber,

                'created_at'                    => $faker->dateTimeBetween($startDate = '- 200 days', $endDate = 'now'),

            ]);
        }
    }
}
