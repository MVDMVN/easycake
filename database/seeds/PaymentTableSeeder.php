<?php

use Illuminate\Database\Seeder;

class PaymentTableSeeder extends Seeder
{

    public function run()
    {

        $faker = Faker\Factory::create();

        {
            DB::table('payment_methods')->insert([
                'method' => 'Visa',
                'created_at' => $faker->dateTimeBetween($startDate = '- 250 days', $endDate = 'now'),
            ]);
            DB::table('payment_methods')->insert([
                'method' => 'MasterCard',
                'created_at' => $faker->dateTimeBetween($startDate = '- 250 days', $endDate = 'now'),
            ]);
            DB::table('payment_methods')->insert([
                'method' => 'Privat',
                'created_at' => $faker->dateTimeBetween($startDate = '- 250 days', $endDate = 'now'),
            ]);
            DB::table('payment_methods')->insert([
                'method' => 'WebMoney',
                'created_at' => $faker->dateTimeBetween($startDate = '- 250 days', $endDate = 'now'),
            ]);
            DB::table('payment_methods')->insert([
                'method' => 'Cash',
                'created_at' => $faker->dateTimeBetween($startDate = '- 250 days', $endDate = 'now'),
            ]);
        }
    }
}
