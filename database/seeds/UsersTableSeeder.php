<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        DB::table('users')->insert([ //,
            'name'       => 'admin',
            'email'      => 'admin@admin.ua',
            'password'   => '$2y$10$dcTxVjkrDa5YUSFnG8oXHOgLYbUlSTLFUxWqFOdnO/TGbQrMMPCbW', /// Password - 111111
            'role' => 1,
            'phone'      => $faker->phoneNumber,
            'created_at' => $faker->dateTimeBetween($startDate = '- 250 days', $endDate = 'now'),
        ]);

        $limit = 30;

        for ($i = 0; $i < $limit; $i++) {
            DB::table('users')->insert([ //,
                'name'       => $faker->name,
                'email'      => $faker->unique()->email,
                'password'   => '$2y$10$dcTxVjkrDa5YUSFnG8oXHOgLYbUlSTLFUxWqFOdnO/TGbQrMMPCbW', /// Password - 111111
                'phone'      => $faker->phoneNumber,
                'created_at' => $faker->dateTimeBetween($startDate = '- 250 days', $endDate = 'now'),
            ]);
        }
    }
}
