<?php

use Illuminate\Database\Seeder;

class CreamsTableSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $faker = Faker\Factory::create();
        {
            DB::table('creams')->insert([
                'cream' => 'Ванильный',
                'created_at' => $faker->dateTimeBetween($startDate = '- 250 days', $endDate = 'now'),
            ]);
            DB::table('creams')->insert([
                'cream' => 'Шоколадный',
                'created_at' => $faker->dateTimeBetween($startDate = '- 250 days', $endDate = 'now'),
            ]);
            DB::table('creams')->insert([
                'cream' => 'Клубничный',
                'created_at' => $faker->dateTimeBetween($startDate = '- 250 days', $endDate = 'now'),
            ]);
            DB::table('creams')->insert([
                'cream' => 'Йогуртовый',
                'created_at' => $faker->dateTimeBetween($startDate = '- 250 days', $endDate = 'now'),
            ]);
            DB::table('creams')->insert([
                'cream' => 'Заварной',
                'created_at' => $faker->dateTimeBetween($startDate = '- 250 days', $endDate = 'now'),
            ]);
        }
    }
}
