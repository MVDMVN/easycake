<?php

use Illuminate\Database\Seeder;

class InfoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        {
            DB::table('infos')->insert([
                'title' => "Доставка",
                'info' => "Идейные соображения высшего порядка, а также дальнейшее развитие различных форм деятельности позволяет оценить значение системы обучения кадров, соответствует насущным потребностям. Идейные соображения высшего порядка, а также консультация с широким активом способствует подготовки и реализации новых предложений.",
            'created_at'                    => $faker->dateTimeBetween($startDate = '- 200 days', $endDate = 'now'),
            ]);
            DB::table('infos')->insert([
                'title' => "Оплата",
                'info' => "С другой стороны реализация намеченных плановых заданий обеспечивает широкому кругу (специалистов) участие в формировании существенных финансовых и административных условий. Идейные соображения высшего порядка, а также реализация намеченных плановых заданий в значительной степени обуславливает создание дальнейших направлений развития. С другой стороны новая модель организационной деятельности играет важную роль в формировании новых предложений. Идейные соображения высшего порядка, а также реализация намеченных плановых заданий обеспечивает широкому кругу (специалистов) участие в формировании направлений прогрессивного развития. Таким образом реализация намеченных плановых заданий влечет за собой процесс внедрения и модернизации модели развития.",
        	'created_at'                    => $faker->dateTimeBetween($startDate = '- 200 days', $endDate = 'now'),
            ]);
            DB::table('infos')->insert([
                'title' => "Гарантии",
                'info' => "Идейные соображения высшего порядка, а также реализация намеченных плановых заданий представляет собой интересный эксперимент проверки модели развития. Разнообразный и богатый опыт реализация намеченных плановых заданий обеспечивает широкому кругу (специалистов) участие в формировании форм развития.Идейные соображения высшего порядка, а также реализация намеченных плановых заданий способствует подготовки и реализации существенных финансовых и административных условий. Таким образом консультация с широким активом позволяет оценить значение форм развития. Товарищи! начало повседневной работы по формированию позиции способствует подготовки и реализации форм развития. С другой стороны начало повседневной работы по формированию позиции обеспечивает широкому кругу (специалистов) участие в формировании новых предложений.",
        	'created_at'                    => $faker->dateTimeBetween($startDate = '- 200 days', $endDate = 'now'),
            ]);
        }
    }
}
