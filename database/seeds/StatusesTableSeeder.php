<?php

use Illuminate\Database\Seeder;

class StatusesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
      {
           DB::table('statuses')->insert([
               'status_type' => 'В обработке',
               'created_at' => $faker->dateTimeBetween($startDate = '- 250 days', $endDate = 'now'),
           ]);
           DB::table('statuses')->insert([
               'status_type' => 'Не просмотрен',
               'created_at' => $faker->dateTimeBetween($startDate = '- 250 days', $endDate = 'now'),
           ]);
           DB::table('statuses')->insert([
               'status_type' => 'Не подтверждён',
               'created_at' => $faker->dateTimeBetween($startDate = '- 250 days', $endDate = 'now'),
           ]);
           DB::table('statuses')->insert([
               'status_type' => 'Ожидает отправки',
               'created_at' => $faker->dateTimeBetween($startDate = '- 250 days', $endDate = 'now'),
           ]);
           DB::table('statuses')->insert([
               'status_type' => 'Исполнено',
               'created_at' => $faker->dateTimeBetween($startDate = '- 250 days', $endDate = 'now'),
           ]);
          DB::table('statuses')->insert([
               'status_type' => 'Отменён',
               'created_at' => $faker->dateTimeBetween($startDate = '- 250 days', $endDate = 'now'),
           ]);
      }
    }
}
