<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        {
            DB::table('categories')->insert([
                'title'      => 'Мужские',
                'photo'      => "/some/cake_" . $faker->numberBetween($min = 1, $max = 4) . ".jpg",
                'created_at' => $faker->dateTimeBetween($startDate = '- 250 days', $endDate = 'now'),
            ]);
            DB::table('categories')->insert([
                'title'      => 'Женские',
                'photo'      => "/some/cake_" . $faker->numberBetween($min = 1, $max = 4) . ".jpg",
                'created_at' => $faker->dateTimeBetween($startDate = '- 250 days', $endDate = 'now'),
            ]);
            DB::table('categories')->insert([
                'title'      => 'Детские',
                'photo'      => "/some/cake_" . $faker->numberBetween($min = 1, $max = 4) . ".jpg",
                'created_at' => $faker->dateTimeBetween($startDate = '- 250 days', $endDate = 'now'),
            ]);
            DB::table('categories')->insert([
                'title'      => 'Юбилейные',
                'photo'      => "/some/cake_" . $faker->numberBetween($min = 1, $max = 4) . ".jpg",
                'created_at' => $faker->dateTimeBetween($startDate = '- 250 days', $endDate = 'now'),
            ]);
            DB::table('categories')->insert([
                'title'      => 'Свадебные',
                'photo'      => "/some/cake_" . $faker->numberBetween($min = 1, $max = 4) . ".jpg",
                'created_at' => $faker->dateTimeBetween($startDate = '- 250 days', $endDate = 'now'),
            ]);
            DB::table('categories')->insert([
                'title'      => 'Разное',
                'photo'      => "/some/cake_" . $faker->numberBetween($min = 1, $max = 4) . ".jpg",
                'created_at' => $faker->dateTimeBetween($startDate = '- 250 days', $endDate = 'now'),
            ]);
            DB::table('categories')->insert([
                'title'      => 'Тематические',
                'photo'      => "/some/cake_" . $faker->numberBetween($min = 1, $max = 4) . ".jpg",
                'created_at' => $faker->dateTimeBetween($startDate = '- 250 days', $endDate = 'now'),
            ]);
        }
    }
}
