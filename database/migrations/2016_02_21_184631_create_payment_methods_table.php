<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentMethodsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
       Schema::create('payment_methods', function (Blueprint $table) {
           $table->increments('id');
           $table->string('method')->unique();
           $table->string('editor');
           $table->timestamp('created_at');
           $table->timestamp('updated_at');
       });
     }

     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
         Schema::drop('payment_methods');
     }
}
