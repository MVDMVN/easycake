<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStatusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
       Schema::create('statuses', function (Blueprint $table) {
           $table->increments('id');
           $table->string('status_type')->unique();
           $table->string('editor');
           $table->timestamp('created_at');
           $table->timestamp('updated_at');
       });
     }

     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
         Schema::drop('statuses');
     }
}
