<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProductsTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->unique();

            $table->integer('id_category')->unsigned();
            $table->foreign('id_category')->references('id')->on('categories');

            $table->integer('id_cream')->unsigned();
            $table->foreign('id_cream')->references('id')->on('creams');

            $table->integer('id_shortcake')->unsigned();
            $table->foreign('id_shortcake')->references('id')->on('shortcakes');

            $table->integer('weight');
            $table->integer('count');
            $table->string('photo');
            $table->string('description')->unique();
            $table->integer('price');
            $table->boolean('availability');
            $table->string('editor');
            $table->timestamp('created_at');
            $table->timestamp('updated_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('products');
    }
}
