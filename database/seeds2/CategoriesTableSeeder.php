<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      {
           DB::table('categories')->insert([
               'title' => 'Мужские'
           ]);
           DB::table('categories')->insert([
               'title' => 'Женские'
           ]);
           DB::table('categories')->insert([
               'title' => 'Детские'
           ]);
           DB::table('categories')->insert([
               'title' => 'Юбилейные'
           ]);
           DB::table('categories')->insert([
               'title' => 'Свадебные'
           ]);
           DB::table('categories')->insert([
               'title' => 'Разное'
           ]);
           DB::table('categories')->insert([
               'title' => 'Тематические'
           ]);
      }
    }
}
