<?php

use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $faker = Faker\Factory::create();

        $limit = 200;

        for ($i = 0; $i < $limit; $i++) {
            DB::table('products')->insert([ //,
                'title'        => $faker->name,
                'id_category'  => $faker->numberBetween($min = 1, $max = 7),
                'id_cream'     => $faker->numberBetween($min = 1, $max = 5),
                'id_shortcake' => $faker->numberBetween($min = 1, $max = 5),
                'weight'       => $faker->numberBetween($min = 2, $max = 5),
                'count'        => $faker->numberBetween($min = 1, $max = 3),
                // 'photo' => $faker->imageUrl($width = 240, $height = 160),
                'photo'        => "/some/cake_4.jpg",
                'description'  => $faker->realText($maxNbChars = 150, $indexSize = 2),
                'price'        => $faker->numberBetween($min = 100, $max = 500),
                'availability' => $faker->numberBetween($min = 0, $max = 1),
            ]);
        }
    }
}
