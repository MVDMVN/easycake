<?php

use Illuminate\Database\Seeder;

class ShortcakesTableSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        {
            DB::table('shortcakes')->insert([
                'shortcake' => 'Шоколадный',
            ]);
            DB::table('shortcakes')->insert([
                'shortcake' => 'Кокосовый',
            ]);
            DB::table('shortcakes')->insert([
                'shortcake' => 'Ванильный',
            ]);
            DB::table('shortcakes')->insert([
                'shortcake' => 'Творожный',
            ]);
            DB::table('shortcakes')->insert([
                'shortcake' => 'Медовый',
            ]);
        }
    }
}
