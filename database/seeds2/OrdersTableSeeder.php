<?php

use Illuminate\Database\Seeder;

class OrdersTableSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $faker = Faker\Factory::create();

        $limit = 100;

        for ($i = 0; $i < $limit; $i++) {
            $ii = $i;
            DB::table('orders')->insert([ //,
                'id'               => ++$ii,
                'id_user'          => $faker->numberBetween($min = 1, $max = 15),
                'id_product'       => $faker->numberBetween($min = 1, $max = 15),
                'id_payment'       => $faker->numberBetween($min = 1, $max = 3),
                'id_status'        => $faker->numberBetween($min = 1, $max = 5),
                'count'            => $faker->numberBetween($min = 1, $max = 4),
                'expected_date'    => $faker->dateTimeBetween($startDate = 'now', $endDate = '+ 15 days'),
                'shipping_address' => $faker->address,
            ]);
        }
    }
}
