<?php

use Illuminate\Database\Seeder;

class PaymentTableSeeder extends Seeder
{

 public function run()
    {
      {
           DB::table('payment_methods')->insert([
               'method' => 'Visa'
           ]);
           DB::table('payment_methods')->insert([
               'method' => 'MasterCard'
           ]);
           DB::table('payment_methods')->insert([
               'method' => 'Privat'
           ]);
           DB::table('payment_methods')->insert([
               'method' => 'WebMoney'
           ]);
           DB::table('payment_methods')->insert([
               'method' => 'Cash'
           ]);
      }
    }
}




