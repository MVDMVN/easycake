<?php

use Illuminate\Database\Seeder;

class CreamsTableSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        {
            DB::table('creams')->insert([
                'cream' => 'Ванильный',
            ]);
            DB::table('creams')->insert([
                'cream' => 'Шоколадный',
            ]);
            DB::table('creams')->insert([
                'cream' => 'Клубничный',
            ]);
            DB::table('creams')->insert([
                'cream' => 'Йогуртовый',
            ]);
            DB::table('creams')->insert([
                'cream' => 'Заварной',
            ]);
        }
    }
}
