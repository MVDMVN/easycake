<?php

use Illuminate\Database\Seeder;

class StatusesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      {
           DB::table('statuses')->insert([
               'status_type' => 'В обработке'
           ]);
           DB::table('statuses')->insert([
               'status_type' => 'Не просмотрен'
           ]);
           DB::table('statuses')->insert([
               'status_type' => 'Не подтверждён'
           ]);
           DB::table('statuses')->insert([
               'status_type' => 'Ожидает отправки'
           ]);
           DB::table('statuses')->insert([
               'status_type' => 'Исполнено'
           ]);
      }
    }
}
