<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $faker = Faker\Factory::create();

     $limit = 15;

     for ($i = 0; $i < $limit; $i++) {
         DB::table('users')->insert([ //,
             'name' => $faker->name,
             'email' => $faker->unique()->email,
             'password' => '$2y$10$dcTxVjkrDa5YUSFnG8oXHOgLYbUlSTLFUxWqFOdnO/TGbQrMMPCbW', /// Password - 111111
             'phone' => $faker->phoneNumber
         ]);
     }
    }
}
