<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOrdersTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('orders', function (Blueprint $table) {
            $table->integer('id');

            $table->integer('id_user')->unsigned();
            $table->foreign('id_user')->references('id')->on('users');

            $table->integer('id_product')->unsigned();
            $table->foreign('id_product')->references('id')->on('products');

            $table->integer('id_payment')->unsigned();
            $table->foreign('id_payment')->references('id')->on('payment_methods');

            $table->integer('id_status')->unsigned();
            $table->foreign('id_status')->references('id')->on('statuses');

            $table->integer('count');
            $table->timestamp('expected_date');
            $table->string('shipping_address');
            $table->string('shipping_address_house_number');
            $table->string('shipping_address_room_number');
            $table->string('shipping_additional_phone');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('orders');
    }
}
