'use strict';

app

  .controller('CategoriesCtrl', ['$scope', '$log', '$http', '$state', '$stateParams', '$firebaseArray', '$firebaseObject', 'FBURL',
  function($scope, $log, $http, $state, $firebaseArray, $firebaseObject, FBURL) {

    // General database variable
    // var ref = new Firebase(FBURL);

    // $scope.categoriesObject = $firebaseObject(ref.child('categories'));
    //////////////////////////// *General database variable




  }
])

.controller('CategoriesListCtrl', ['$scope', '$log', '$http', '$state', '$filter', 'ngTableParams', 'toastr',
  function($scope, $log, $http, $state, $filter, ngTableParams, toastr) {

    //////////////////////////////////////////
    //************ Table Settings **********//
    //////////////////////////////////////////


    $scope.myCategory = function() {
      $http.get('/admin/category')
        .success(function(result) {
          var data = result.obj;
          $scope.categories = result.obj;
          console.log($scope.categories);

          $scope.tableParams = new ngTableParams({
            page: 1, // show first page
            count: 10, // count per page
            sorting: {
              id: 'asc' // initial sorting
            }
          }, {
            total: data.length, // length of data
            getData: function($defer, params) {
              // use build-in angular filter
              var orderedData = params.sorting() ?
                $filter('orderBy')(data, params.orderBy()) :
                data;

              orderedData = $filter('filter')(orderedData, $scope.searchText);
              params.total(orderedData.length);

              $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
            }
          });
        });
    }

    $scope.myCategory();

    // watch data in scope, if change reload table
    $scope.$watchCollection('categories', function(newVal, oldVal) {
      if (newVal !== oldVal) {
        $scope.tableParams.reload();
      }
    });

    $scope.$watch('searchText', function(newVal, oldVal) {
      if (newVal !== oldVal) {
        $scope.tableParams.reload();
      }
    });
    ///////////////////////////////////////////// *watch data in scope, if change reload table


    // Delete CRUD operation
    $scope.delete = function(id) {
      var idData = "id=" + id;
      if (confirm('Вы уверены?')) {
        $http.delete("/admin/category", {
            data: idData,
            headers: {
              'Content-Type': 'application/x-www-form-urlencoded'
            }
          })
          .success(function(data) {
            console.log(data);
            toastr.success('Удаление категории', 'Категория была удалена!');
            $state.go('app.categories.list', {}, {
              reload: true
            });
          })
      }
    };
    //////////////////////////// *Delete CRUD operation


  }
])

.controller('NewCategoryCtrl', ['$scope', '$http', 'toastr', '$state', 'FBURL', '$filter', 'Upload',
    function($scope, $http, toastr, $state, FBURL, $filter, Upload) {

      $scope.categoryAdd = function(title, file) {
          console.log(title);
          console.log(file[0]);

        Upload.upload({
          url: '/admin/category/add',
          data: {title, file: file[0]},
          headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
              }
        })
        .success(function (result){
          toastr.success('Добавление категории', 'Категория была добавлена!');
          $state.go('app.categories.list', {}, {
            reload: true
          });
        })
      }
    }
  ])
  /////////////////////// *Submit operation


.controller('EditCategoryCtrl', ['$scope', '$http', '$stateParams', '$firebaseObject', 'toastr', '$state', 'FBURL', '$filter', 'Upload',
  function($scope, $http, $stateParams, $firebaseObject, toastr, $state, FBURL, $filterm, Upload) {
      var id = "id=" + $stateParams.id;
      $http( {
          method: 'POST',
          url: "/admin/category/card",
          data: id,
          headers: {
              'Content-Type': 'application/x-www-form-urlencoded'
          },
      } )
          .success(function(result) {
              $scope.category = result[0];
              console.log($scope.category);
          })

    $scope.categoryUpdate = function(category, file=null) {
        console.log(file);
        Upload.upload({
            url: '/admin/category/update',
            data: {category, file: file[0]},
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        }).success(function(result){
            toastr.success('Редактирование категории', 'Категория была отредактирована!');
            $state.go('app.categories.list', {}, {
              reload: true
            });
        })
    }
  }
])

.controller('ShowCategoryCtrl', ['$scope', '$http', '$firebaseObject', 'toastr', '$state', 'FBURL', '$stateParams',
  function($scope, $http, $firebaseObject, toastr, $state, FBURL, $stateParams) {


        var id = "id=" + $stateParams.id;
        $http( {
            method: 'POST',
            url: "/admin/category/card",
            data: id,
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
        } )
            .success(function(result) {
                $scope.category = result[0];
                console.log($scope.category);
            })
  }
]);
