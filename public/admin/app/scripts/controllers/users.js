'use strict';

app

  .controller('UsersCtrl', ['$scope', '$log', '$http', '$stateParams', '$state', '$filter', 'ngTableParams', 'toastr',
    function($scope, $state, $stateParams, $firebaseArray, $firebaseObject, FBURL) {
    }])

  .controller('UsersListCtrl', ['$scope', '$log', '$http', '$stateParams', '$state', '$filter', 'ngTableParams', 'toastr',
    function($scope, $log, $http, $stateParams, $state, $filter, ngTableParams, toastr) {


      //////////////////////////////////////////
      //************ Table Settings **********//
      //////////////////////////////////////////

      $scope.myUser = function(){
          $http.get( '/admin/user' )
          .success( function(result){
            var data = result;

        $scope.tableParams = new ngTableParams({
          page: 1,            // show first page
          count: 10,          // count per page
          sorting: {
            role: 'desc'     // initial sorting
          }
        }, {
          total: data.length, // length of data
          getData: function($defer, params) {
            // use build-in angular filter
            var orderedData = params.sorting() ?
              $filter('orderBy')(data, params.orderBy()) :
              data;

            orderedData = $filter('filter')(orderedData, $scope.searchText);
            params.total(orderedData.length);

            $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
          }
        });
          });
      }

      $scope.myUser();


        $scope.$watchCollection('users', function(newVal, oldVal){
          if (newVal !== oldVal) {
            $scope.tableParams.reload();
          }
        });

        $scope.$watch('searchText', function(newVal, oldVal){
          if (newVal !== oldVal) {
            $scope.tableParams.reload();
          }
        });


      $scope.delete = function(id) {
        var idData = "id=" + id;
        console.log(id);
        if (confirm('Вы уверены?')) {
          $http.delete("/admin/user", {
                data: idData,
                headers: {
                  'Content-Type': 'application/x-www-form-urlencoded'
                }
              })
              .success(function(data) {
                console.log(data);
                toastr.success('Удаление пользователя', 'Пользователь был удален!');
                $state.go('app.users.list', {}, {
                  reload: true
                });
              })
        }
      };

        $scope.userRole = [{
            'id' : 1,
            'role' : 'Администратор'
            },{
            'id': 0,
            'role': 'Пользователь'
            }
        ];

        $scope.roleShow = function (id) {

            if(id == 1){
                return "Администратор"
            }else{
                return "Пользователь"
            }

        }

        $scope.changeRole = function(userID, roleID){


            var roleUpdateData = 'userID=' + userID + '&'
                + 'roleID=' + roleID;

            console.log(roleUpdateData);

            $http( {
                method: 'POST',
                url: "/admin/user/roleUpdate",
                data: roleUpdateData,
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
            } )
                .success(function(result) {
                    toastr.success('Добавление администратора', 'Новый администратор был добавлен!');
                    $state.go('app.users.list', {}, {
                        reload: true
                    });
                })


        };

    }])

  // .controller('EditUserCtrl', ['$scope', '$firebaseObject', 'toastr', '$state', 'FBURL', '$filter',
  //   function($scope, $firebaseObject, toastr, $state, FBURL, $filter) {
  //
  //     $scope.editing = true;
  //
  //     var ref = new Firebase(FBURL);
  //     var profiles = ref.child('users');
  //
  //     $scope.user.$loaded().then(function(){
  //       $scope.oldEmail = angular.copy($scope.user.email);
  //     });
  //
  //     $scope.users.$loaded(function(){
  //
  //       // Submit operation
  //       $scope.ok = function() {
  //
  //         $scope.userEntry = {
  //           name: $scope.user.name,
  //           email: $scope.user.email,
  //           role: $scope.user.role,
  //           address: {
  //             street: $scope.user.address.street,
  //             city: $scope.user.address.city,
  //             zip: $scope.user.address.zip,
  //             country: $scope.user.address.country
  //           },
  //           phone: $scope.user.phone
  //         };
  //
  //         $scope.credentials = {
  //           password: $scope.user.password,
  //           oldpassword: $scope.user.oldpassword
  //         };
  //
  //         var updateOnSuccess = function() {
  //           profiles.child($scope.user.$id).update($scope.userEntry, function() {
  //             toastr.success('User has been saved', 'User Saved!');
  //             $state.go('app.users.list', {}, {reload: true});
  //           });
  //         };
  //
  //         var changeEmail = $scope.user.changeEmail;
  //         var changePass = $scope.user.changePass;
  //
  //         if (changeEmail === true) {
  //           ref.changeEmail({
  //             oldEmail : $scope.oldEmail,
  //             newEmail : $scope.user.email,
  //             password : $scope.credentials.password
  //           }, function(error) {
  //             if (error === null) {
  //               console.log("Email changed successfully");
  //               toastr.success('Email has been changed successfully', 'Email changed!');
  //               updateOnSuccess();
  //             } else {
  //               toastr.error(error.message, 'Email change error!');
  //               console.log("Error changing email:", error);
  //             }
  //           });
  //         } else if (changePass === true) {
  //           ref.changePassword({
  //             email       : $scope.user.email,
  //             oldPassword : $scope.credentials.oldpassword,
  //             newPassword : $scope.credentials.password
  //           }, function(error) {
  //             if (error === null) {
  //               console.log("Password changed successfully");
  //               toastr.success('Password has been changed successfully', 'Password changed!');
  //               updateOnSuccess();
  //             } else {
  //               toastr.error(error.message, 'Password change error!');
  //               console.log("Error changing password:", error);
  //             }
  //           });
  //         } else {
  //           updateOnSuccess();
  //         }
  //       };
  //       /////////////////////// *Submit operation
  //     });
  //
  //
  //   }])

  .controller('ShowUserCtrl', ['$scope', '$firebaseObject', 'toastr', '$state', 'FBURL', '$stateParams',
    function($scope, $firebaseObject, toastr, $state, FBURL, $stateParams) {


    }]);
