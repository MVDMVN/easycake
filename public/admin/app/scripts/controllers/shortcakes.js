'use strict';

app

  .controller('ShortcakesCtrl', ['$scope', '$log', '$http', '$state', '$stateParams', '$firebaseArray', '$firebaseObject', 'FBURL',
  function($scope, $log, $http, $state, $firebaseArray, $firebaseObject, FBURL) {
  }
])

.controller('ShortcakesListCtrl', ['$scope', '$log', '$http', '$state', '$filter', 'ngTableParams', 'toastr',
  function($scope, $log, $http, $state, $filter, ngTableParams, toastr) {

    $scope.myShortcake = function() {
      $http.get('/admin/shortcakes')
        .success(function(result) {
          var data = result.obj;
          $scope.shortcakes = result.obj;
          console.log($scope.shortcakes);


          $scope.tableParams = new ngTableParams({
            page: 1, // show first page
            count: 10, // count per page
            sorting: {
              id: 'asc' // initial sorting
            }
          }, {
            total: data.length, // length of data
            getData: function($defer, params) {
              // use build-in angular filter
              var orderedData = params.sorting() ?
                $filter('orderBy')(data, params.orderBy()) :
                data;

              orderedData = $filter('filter')(orderedData, $scope.searchText);
              params.total(orderedData.length);

              $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
            }
          });
        });
    }

    $scope.myShortcake();


    $scope.$watchCollection('categories', function(newVal, oldVal) {
      if (newVal !== oldVal) {
        $scope.tableParams.reload();
      }
    });

    $scope.$watch('searchText', function(newVal, oldVal) {
      if (newVal !== oldVal) {
        $scope.tableParams.reload();
      }
    });


    $scope.delete = function(id) {
      var idData = "id=" + id;
      if (confirm('Вы уверены?')) {
        $http.delete("/admin/shortcake/delete", {
            data: idData,
            headers: {
              'Content-Type': 'application/x-www-form-urlencoded'
            }
          })
          .success(function(data) {
            console.log(data);
            toastr.success('Удаление коржа', 'Корж был удалён!');
            $state.go('app.shortcakes.list', {}, {
              reload: true
            });
          })
      }
    };
    //////////////////////////// *Delete CRUD operation


  }
])

.controller('NewShortcakeCtrl', ['$scope', '$http', 'toastr', '$state', 'FBURL', '$filter', 'Upload',
    function($scope, $http, toastr, $state, FBURL, $filter, Upload) {
      $scope.shortcakesAdd = function(shortcake) {

        Upload.upload({
          url: '/admin/shortcake/add',
          data: {shortcake},
          headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
              }
        })
        .success(function (result){
                    toastr.success('Добавление коржа', 'Корж был добавлен!');
          $state.go('app.shortcakes.list', {}, {
            reload: true
          });
        })
      }
    }
  ])
  /////////////////////// *Submit operation


.controller('EditShortcakeCtrl', ['$scope', '$http', '$stateParams', '$firebaseObject', 'toastr', '$state', 'FBURL', '$filter',
  function($scope, $http, $stateParams, $firebaseObject, toastr, $state, FBURL, $filter) {
    var id = $stateParams.id;

    $http.get('/admin/shortcakes')
      .success(function(result) {
        $scope.shortcake = result.arr[id] ;
        console.log($scope.shortcake);
      });

    $scope.shortcakesUpdate = function(shortcake) {
      var updateData = "id=" + id + "&" + "shortcake=" + shortcake;
      $http( {
        method: 'POST',
        url: "/admin/shortcake/update",
        data: updateData,
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        },
      } )
        .success(function(result) {
          toastr.success('Редактирование коржа', 'Корж был изменён!');
          $state.go('app.shortcakes.list', {}, {
            reload: true
          });
        })
    }
  }
])

.controller('ShowShortcakeCtrl', ['$scope', '$http', '$firebaseObject', 'toastr', '$state', 'FBURL', '$stateParams',
  function($scope, $http, $firebaseObject, toastr, $state, FBURL, $stateParams) {

        var id = "id=" + $stateParams.id;
        $http( {
            method: 'POST',
            url: "/admin/category/card",
            data: id,
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
        } )
            .success(function(result) {
                $scope.category = result[0];
                console.log($scope.category);
            })
  }
]);
