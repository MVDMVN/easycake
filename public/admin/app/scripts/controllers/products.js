'use strict';

app

  .controller('ProductsCtrl', ['$scope', '$log', '$state', '$http', '$stateParams', '$filter', 'uploadImage',
    function($scope, $log, $state, $http, $stateParams, $filter, uploadImage) {



    }])

  .controller('ProductsListCtrl', ['$scope', '$log', '$http', '$state', '$filter', 'ngTableParams', 'toastr',
    function($scope, $log, $http, $state, $filter, ngTableParams, toastr) {


      //////////////////////////////////////////
      //************ Table Settings **********//
      //////////////////////////////////////////

      $scope.myProduct = function(){

        $http.get('/admin/product')
          .success(function(result) {
            var data = result.obj;
            $scope.products = result.obj;
});


          $http.get( '/admin/product' )
          .success( function(result){
            var data = result.obj;
            angular.forEach(data, function (value, key) {
              data[key].created_at = new Date(value.created_at);
            });
            console.log(data);

        $scope.tableParams = new ngTableParams({
          page: 1,            // show first page
          count: 10,          // count per page
          sorting: {
            id: 'asc'     // initial sorting
          }
        }, {
          total: data.length, // length of data
          getData: function($defer, params) {
            // use build-in angular filter
            var orderedData = params.sorting() ?
              $filter('orderBy')(data, params.orderBy()) :
              data;

            orderedData = $filter('filter')(orderedData, $scope.searchText);
            params.total(orderedData.length);

            $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
          }
        });
          });
      };

      $scope.myProduct();


        $scope.$watchCollection('product', function(newVal, oldVal){
          if (newVal !== oldVal) {
            extendArray();
            $scope.tableParams.reload();
          }
        });

        $scope.$watch('searchText', function(newVal, oldVal){
          if (newVal !== oldVal) {
            $scope.tableParams.reload();
          }
        });

        $scope.delete = function(id) {
          var idData = "id=" + id;
          console.log(id);
          if (confirm('Вы уверены?')) {
            $http.delete("/admin/product", {
                data: idData,
                headers: {
                  'Content-Type': 'application/x-www-form-urlencoded'
                }
              })
              .success(function(data) {
                console.log(data);
                toastr.success('Удаление товара', 'Товар был удален!');
                $state.go('app.products.list', {}, {
                  reload: true
                });
              })
          }
        };

    }])

  .controller('NewProductCtrl', ['$scope', '$http', '$stateParams', '$firebaseObject', 'toastr', '$state', 'Upload',
    function($scope, $http, $stateParams, $firebaseObject, toastr, $state, Upload ) {

        $http.get('/admin/product')
            .success(function(result) {
                $scope.product = result.obj[id-1];
            });

        $http.get('/admin/category')
            .success(function(result) {
                $scope.categories = result.obj;

            });

        $http.get('/admin/product/option')
            .success(function(result) {
                $scope.creams = result.cream;
                $scope.cakes = result.cake;

            });

        $scope.productAdd = function(product, file) {
          Upload.upload({
            url: '/admin/product/add',
            data: {product, file: file[0]},
            headers: {
                  'Content-Type': 'application/x-www-form-urlencoded'
                }
          }).success(function(result){
              toastr.success('Добавление товара', 'Товар был добавлен!');
              $state.go('app.products.list', {}, {
                  reload: true
              });
          });


            // $http( {
            //     method: 'POST',
            //     url: "/admin/product/add",
            //     data: addData,
            //     headers: {
            //         'Content-Type': 'application/x-www-form-urlencoded'
            //     },
            // } )
            //     .success(function(result) {
            //         // toastr.success('Добавление товара', 'Товар был добавлен!');
            //         // $state.go('app.products.list', {}, {
            //         //     reload: true
            //         // });
            //
            //     })
        }


    }])

  .controller('EditProductCtrl', ['$scope', '$http', '$stateParams', '$firebaseObject', 'toastr', '$state', 'Upload',
    function($scope, $http, $stateParams, $firebaseObject, toastr, $state, Upload) {
      var id = "id=" + $stateParams.id;
      $http( {
        method: 'POST',
        url: "/admin/product/card",
        data: id,
        cache: false,
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        },
      } )
        .success(function(result) {
            $scope.product = result[0];
            console.log($scope.product);
        })

        $http.get('/admin/category')
        .success(function(result) {
            $scope.categories = result.obj;
            console.log($scope.categories);
        });

        $http.get('/admin/product/option')
        .success(function(result) {
            $scope.creams = result.cream;
            $scope.cakes = result.cake;
            console.log($scope.creams);
            console.log($scope.cakes);
        });



      $scope.productUpdate = function(product, file) {
          console.log(file[0]);
          Upload.upload({
              url: '/admin/product/update',
              data: {product, file: file[0]},
              headers: {
                  'Content-Type': 'application/x-www-form-urlencoded'
              }
          }).success(function(result){
              toastr.success('Редактирование товара', 'Товар был отредактирован!');
              $state.go('app.products.list', {}, {
                  reload: true
              });
          })


      }
    }])

  .controller('ShowProductCtrl', ['$scope', '$http', '$firebaseObject', 'toastr', '$state', 'FBURL', '$stateParams',
    function($scope, $http, $firebaseObject, toastr, $state, FBURL, $stateParams) {

        var id = "id=" + $stateParams.id;
        $http( {
            method: 'POST',
            url: "/admin/product/card",
            data: id,
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
        } )
            .success(function(result) {
                $scope.product = result[0];
              $scope.product.created_at = new Date($scope.product.created_at);

            })
    }]);
