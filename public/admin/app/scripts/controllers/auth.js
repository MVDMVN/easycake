'use strict';

app.controller('AuthCtrl', ['$scope', 'Auth', '$state', 'fbutil', 'FBURL', '$firebaseArray', 'toastr', function($scope, Auth, $state, fbutil, FBURL, $firebaseArray, toastr) {

  toastr.success('demo@minovate.com, pass: demo', 'Login default data!', {progressBar: true, timeOut: '15000'});

  //redirect if user is logged in
  
     $state.go('app.dashboard', {}, {reload: true});
  

  $scope.email = null;
  $scope.pass = null;
  $scope.confirm = null;
  $scope.createMode = false;
  $scope.rememberMe = true;

  $scope.login = function() {

$state.go('app.dashboard', {}, {reload: true});

    // $scope.err = null;
    // Auth.$authWithPassword({ email: email, password: pass }, {rememberMe: rememberMe})
    //   .then(function(authData) {
    //     console.log("Authenticated successfully with payload:", authData);
    //     $state.go('app.dashboard', {}, {reload: true});
    //   }, function(err) {
    //     $scope.err = errMessage(err);
    //     console.log("Login Failed!", err);
    //   });
  };

  $scope.register = function() {
    $scope.err = null;
    if( assertValidAccountProps() ) {
      var email = $scope.email;
      var pass = $scope.pass;
      // create user credentials in Firebase auth system
      Auth.$createUser({email: email, password: pass})
        .then(function() {
          // authenticate so we have permission to write to Firebase
          return Auth.$authWithPassword({ email: email, password: pass });
        })
        .then(function(user) {

          // create a user profile in our data store
          var ref = fbutil.ref('users', user.uid);
          fbutil.handler(function(cb) {
            ref.update({email: email, role: "user"}, cb);
          });

        })
        .then(function(/* user */) {
          $state.go('app.dashboard', {}, {reload: true});
        }, function(err) {
          $scope.err = errMessage(err);
        });
    }
  };

  function assertValidAccountProps() {
    if( !$scope.email ) {
      $scope.err = 'Введите email';
    }
    else if( !$scope.pass || !$scope.confirm ) {
      $scope.err = 'Введите парль';
    }
    else if( $scope.createMode && $scope.pass !== $scope.confirm ) {
      $scope.err = 'Неправильный пароль';
    }
    return !$scope.err;
  }

  function errMessage(err) {
    return angular.isObject(err) && err.code? err.code : err + '';
  }
}]);
