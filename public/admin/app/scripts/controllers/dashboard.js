'use strict';

app
  .controller('DashboardCtrl', ['$scope', '$state', '$http', '$stateParams', '$filter', 'uploadImage', 'toastr',
    function($scope, $state, $http, $stateParams, $filter, uploadImage, toastr) {

      $scope.page = {
        title: 'Dashboard'
      };

      $http.get( '/admin/dashboard' )
      .success( function(result){
        $scope.dbData = result;
        console.log($scope.dbData);
});
    }])


  .controller('ProductsChartCtrl', ['$scope', '$filter', '$http',
    function($scope, $filter, $http) {
        var count = 0;

        $scope.options = {
          "chart": {
            "type": "pieChart",
            "height": 500,
            "x" : function (d){return d.key;},
            "y" : function (d){return d.y;},
            "showLabels": true,
            "duration": 500,
            "labelThreshold": 0.01,
            "labelSunbeamLayout": true,
            "legend": {
              "margin": {
                "top": 5,
                "right": 35,
                "bottom": 5,
                "left": 0
              }
            }
          }
      };

        $http.get('/admin/dashboard')
            .success( function(result) {
                $scope.chartData = result.sql;

                count = $scope.chartData.length;

                console.log(count);
                function getRandomColor() {
                    var letters = '0123456789ABCDEF'.split('');
                    var color = '#';
                    for (var i = 0; i < 6; i++ ) {
                        color += letters[Math.floor(Math.random() * 16)];
                    }
                    return color;
                }
                

                  // for(var i = 0; i < count; i++){
                  //     $scope.data = [
                  //     {
                  //         key: $scope.chartData[i].title,
                  //         y: $scope.chartData[i].products_count
                  //     }
                  //     ];
                  //  }


        $scope.data = [
            {
                key: "Мужские",
                y: 15
            },
            {
                key: "Женские",
                y: 2
            },
            {
                key: "Детские",
                y: 9
            },
            {
                key: "Юбилейные",
                y: 7
            },
            {
                key: "Свадебные",
                y: 4
            },
            {
                key: "Разное",
                y: 3
            },
            {
                key: "Тематические",
                y: 10
            }
        ];

                    // angular.forEach($scope.chartData, function(value, key) {
                    //     // $scope.chart[key] = {
                    //     //         label: value.title,
                    //     //         color : getRandomColor(),
                    //     //         highlight : getRandomColor(),
                    //     //         value: value.products_count
                    //     // }
                    //       $scope.data[1] = {
                    //             key: value.title,
                    //             y: value.products_count
                    //     };
                    // });
            })


    }
  ])

.controller('LastCommentsCtrl', ['$scope', '$filter', '$http', 'toastr', '$state',
  function($scope, $filter, $http, toastr, $state) {
    $http.get('/admin/dashboard')
      .success( function(result) {

        $scope.lastComments = result.lastComments;

      })



    $scope.changeStatus = function (commentId, statusId) {

      var statusUpdateData = 'commentId=' + commentId + '&'
        + 'statusId=' + statusId;

      console.log(statusUpdateData);
      $http( {
        method: 'POST',
        url: "/admin/comment/statusUpdate",
        data: statusUpdateData,
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        },
      } )
        .success(function(result) {
          toastr.success('Изменение статуса', 'Статус был изменён!');
          $state.go('app.dashboard', {}, {
            reload: true
          });
        })
    };

  }
]);
