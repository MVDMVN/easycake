var easyCake = angular.module( 'easyCake', [ 'ngMaterial', 'ngMessages', 'ngAnimate', 'angular-notification-icons', 'angular-input-stars', 'slickCarousel', 'duScroll', 'toastr'] );


easyCake.controller( 'homeController', function( $scope, $http) {
  	$http.get('/admin/category')
    	.success(function(result) {
      	var data = result.obj;
      	$scope.categories = result.title;
      	$scope.number = result.title;
      	console.log($scope.categories);


$scope.slickConfig = {
    enabled: true,
    autoplay: true,
    draggable: false,  
    autoplaySpeed: 3000,
    slidesToShow: 5, 
    slidesToScroll: 1,
    method: {},
    event: {
        beforeChange: function (event, slick, currentSlide, nextSlide) {
        },
        afterChange: function (event, slick, currentSlide, nextSlide) {
        }
    }
};

      });


    $scope.numberLoaded = true;
    $scope.numberUpdate = function(){
        $scope.numberLoaded = true;
    };


	$scope.slickConfig = {
		enabled: true,
		autoplay: true,
		draggable: false,  
		autoplaySpeed: 2300,
		slidesToShow: 5, 
		slidesToScroll: 1,
		method: {},
		event: {
		    beforeChange: function (event, slick, currentSlide, nextSlide) {
		    },
		    afterChange: function (event, slick, currentSlide, nextSlide) {
		    }
		}
	};
});

easyCake.factory( 'pagination', function( $log, $http ) {
	return {
		getValue: function( cat ) {
			var category = cat;
			var catUrl = "/торты/" + category + "/api";

			return $http.get( catUrl )
				.success( function( data ) {
					values = data.items;
					creams = data.creams;
				} );

		}
	}

} );


easyCake.filter( 'startFrom', function() {
	return function( input, start ) {
		if ( !input || !input.length ) {
			return;
		}
		start = +start;
		return input.slice( start );
	}
} );


easyCake.controller( 'cartController', function( $scope, $rootScope, $http, $log, pagination, $filter, $mdDialog, $mdMedia, $window, toastr ) {

	$scope.sortType = ''; // значение сортировки по умолчанию
	$scope.sortReverse = false; // обратная сортривка

	$scope.creamSort = function(cream){
		$scope.cr = cream;
		$scope.cr;
	};

	$scope.shortcakeSort = function(shortcake){
		$scope.sh = shortcake;
		$scope.sh;
	};

	$scope.min = 0;
	$scope.max = 1000;

	$scope.Summary = 0;
	$scope.Total = 0;

	$scope.minDate = new Date();

	$scope.name = '';
	$scope.email = '';
	$scope.mess = '';


	var urlAdd = '/корзина';
	var urlRemove = '/корзина/удалить';
	var urlOrderAdd = '/корзина/заказать';

	$scope.sendMail = function(name ,email, mess){

		var dataMail = 'name=' + name + '&'
				+ 'email=' + email + '&'
				+ 'mess=' + mess;

		$http( {
			method: 'POST',
			url: "/контакты/send",
			data: dataMail,
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			},
		} )
			.success( function( data ) {
			toastr.success('Ваше сообщение было успешно отправлено!');			
		});
	}

	$scope.basketInit = function(){
		$http.get('/корзина/количество')
			.success(function(data){
				$scope.basketCount = data;
				console.log($scope.basketCount);
			})
	}

	$scope.showBasket = function() {

		$http.get( '/корзина/full' )
			.success( function( data ) {
				$scope.basketItems = data;
				$scope.Summary = 0;
				for(var i=0; i<$scope.basketItems.length; i++){
					$scope.Sum  = $scope.basketItems[i].price * $scope.basketItems[i].qty;
					$scope.Summary +=  $scope.Sum ;
				}
				$scope.basketEmpty = false;
			} )
			.error( function( data ) {
				$scope.basketEmpty = true;
			} );

	}
	$scope.showBasket();

	$scope.showPayment = function() {
		return $http.get( "корзина/оплата" )
			.success( function( data ) {
				$scope.paymentMethods = data;
			} );
	}


	$scope.qtyUpdate = function( id, qty ) {

		var qtyData =
			'id=' + id + "&" +
			"qty=" + qty;
		$http( {
			method: 'POST',
			url: "/корзина/обновить",
			data: qtyData,
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			},
		} )
			.success( function( data ) {
				$scope.Summary = 0;
				for (var i = 0; i < $scope.basketItems.length; i++) {
					$scope.Sum = $scope.basketItems[i].price * $scope.basketItems[i].qty;
					$scope.Summary = $scope.Summary + $scope.Sum;
				};
			} );
	};

	$scope.addTooCart = function( id ) {

		var dataId = "id=" + id;

		$http( {
			method: 'POST',
			url: urlAdd,
			data: dataId,
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			},
		} )
			.success( function( data ) {

				$scope.basketCount = data;
				$scope.basketInit();
				toastr.success('Товар успешно добавлен в корзину!');

			} )

	}

	  $scope.showAlert = function(ev) {
    $mdDialog.show(
      $mdDialog.alert()
        .parent(angular.element(document.querySelector('#popupContainer')))
        .clickOutsideToClose(true)
        .title('Вы не зарегистрированный пользователь')
        .textContent('Для покупок в магазине Вам необходимо зарегистрироваться.')
        .ariaLabel('Alert Dialog Demo')
        .ok('Ясно')
        .targetEvent(ev)
    );
  };


	$scope.removeFromBasket = function( id ) {
		var dataId = "id=" + id;
		$log.info( dataId );
		$http( {
			method: "POST",
			url: urlRemove,
			data: dataId,
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			},
		} )
			.success( function( data ) {
				$scope.showBasket();
				$scope.basketInit();
				toastr.error('Товар успешно удалён из корзины!');
			} )
	}

	$scope.showCategItems = function( cat ) {
		$scope.cat = cat;
		pagination.getValue( cat )

		.success( function( data ) {
			$scope.categoryItems = data.items;
			$scope.creams = data.creams;
			$scope.shortcakes = data.shortcakes;
			console.log($scope.creams);
			$scope.paginationInit( data );
		} );
	}


	$scope.orderSend = function( qty, additionalPhone, street, houseNumber, apartmentNumber, date) {

        $scope.orderForm = {};
		$scope.orderForm.qty = qty;
		$scope.orderForm.additionalPhone = additionalPhone;
		$scope.orderForm.street = street;
		$scope.orderForm.houseNumber = houseNumber;
		$scope.orderForm.apartmentNumber = apartmentNumber;

        $scope.orderForm.date = date;

        var myDate = $filter('date')($scope.orderForm.date, 'yyyy-MM-dd HH:mm:ss')

		var orderData =
			'qty=' + $scope.basketQty + "&" +
			"additionalPhone=" + $scope.orderForm.additionalPhone + "&" +
			"street=" + $scope.orderForm.street + "&" +
			"houseNumber=" + $scope.orderForm.houseNumber + "&" +
			"apartmentNumber=" + $scope.orderForm.apartmentNumber + "&" +
			"date=" + myDate;


		$http( {
			method: "POST",
			url: urlOrderAdd,
			data: orderData,
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			},
		} )
			.success( function( data ) {
				toastr.success('Мы свяжемся с Вами в ближайшее время!', 'Ваш заказ был принят!');
			} )

	}
	/***PAGINATION***/

	$scope.paginationInit = function( items ) {
		$scope.items = items;
	}

	$scope.currentPage = 0;
	$scope.itemsPerPage = 6;

	$scope.firstPage = function() {
		return $scope.currentPage == 0;
	}
	$scope.lastPage = function() {
		var lastPageNum = Math.ceil( $scope.items.length / $scope.itemsPerPage - 1 );
		return $scope.currentPage == lastPageNum;
	}
	$scope.numberOfPages = function() {
		return Math.ceil( $scope.items.length / $scope.itemsPerPage );
	}
	$scope.startingItem = function() {
		return $scope.currentPage * $scope.itemsPerPage;
	}
	$scope.pageBack = function() {
		$scope.currentPage = $scope.currentPage - 1;
	}
	$scope.pageForward = function() {
		$scope.currentPage = $scope.currentPage + 1;
	}


} );