easyCake.controller( 'CommentCtrl', function( $scope, $mdDialog, $mdMedia) {

    $scope.status = '  ';
    $scope.customFullscreen = $mdMedia('xs') || $mdMedia('sm');

    $scope.showAlert = function(ev) {

         $mdDialog.show(
             $mdDialog.alert()
                 .parent(angular.element(document.querySelector('#popupContainer')))
                 .clickOutsideToClose(true)
                 .title('Комментарий отправлен')
                 .textContent('Ваш комментарий был отпарвлен на проверку.  Он появится как только админстратор удостоверится в вашей порядочности. ')
                 .ok('Понятно')
                 .targetEvent(ev)
          );
    };

});


easyCake.controller( 'RatingCtrl', function( $scope, $http ) {

    $scope.rating = 1;

    $scope.setRating = function(rating,user, product){

        var ratingData = "rating=" + rating + "&" + "user=" + user + "&" + "product=" + product;

            $http( {
                method: 'POST',
                url: "/rating",
                data: ratingData,
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
            } )
                .success( function( data ) {
                    console.log(data);
                } );

    }

    $scope.showRating = function(product){
        $http({
            method: 'POST',
            url: "/ratingShow",
            data: product,
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
        })
            .success(function(data){
                console.log(data);
            });
    }
    
});